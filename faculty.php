<hr>
<h3 id="hits"></h3>
 <ul class="researcher-list" id="faclist">
      
    

<li class="researcher ita europe italianstudies humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Abbona-Sneider, Cristina</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Abbona-Sneider's research and teaching interests include curriculum design, use of technology in language instruction, techniques of teaching advanced level writing, teaching language and culture through film.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cabbona">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher idn asia obstetricsandgynecology biology&medicine" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Adashi, Eli</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Adashi is a recent fellow and Senior Advisor on Global Women's Health to the Secretary of State Office of Global Women's Issues; he is also a member of the Board of Directors of Physicians for Human Rights, a member of the Global Agenda Council on Population Growth of the World Economic Forum, and the chair of the Medical Executive Committee and the Medical Advisory Council of the Jones Foundation for Reproductive Medicine.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eadashi">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Indonesia</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Obstetrics and Gynecology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr yem asia judaicstudies humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Adler Ben Yehuda, Ruth</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Adler Ben Yehuda’s interests include Hebrew language pedagogy, language learning and technology, music in teaching of foreign languages, the history of the Hebrew language, which includes other Semitic languages, and, of course, Judeo-Arabic (especially of the Jews in Yemen).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/radlerbe">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Israel; Yemen</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Judaic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe comparativeliterature frenchstudies humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ahearn, Edward</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Ahearn has research interests in many phases of Comparative Literature and 19th- and 20th-century French literature and poetry, literary theory, and literature and the city.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eahearn">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tur irn afg asia history socialsciences" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ahmed, Faiz</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Ahmed's primary research explores the intersection of religious and educational networks, constitutional movements, and "rule of law" ideology in late Ottoman Turkey, Qajar Iran, and Afghanistan during the long 19th century.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/fa8">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Afghanistan; Iran; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr can aus europe northamerica oceania appliedmathematics physicalsciences" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ainsworth, Mark</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Ainsworth’s research interests are the development of efficient and practical computational algorithms for the adaptive solution of systems of partial differential equations (PDEs), their rigorous mathematical analysis and their application to relevant physical problems. In his work, Ainsworth has developed rigorous bounds to quantify and control the accuracy of simulations of a diverse range of applications from electromagnetics working with BAe Systems, to the certification of pipelines in the North Sea Oil industry.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mainswor">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Australia; Canada; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Applied Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica neuroscience biology&medicine" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Aizenman-Stern, Carlos</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Aizenman’s research aims to understand the role of sensory experience in shaping the connectivity and functional properties of developing neural circuits, as well as its implications for neurodevelopmental disorders.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/caizenma">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Neuroscience</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jor grc arm asia europe archaeologyandtheancientworld classics humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Alcock, Susan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Alcock's research interests are in the material culture of the eastern Mediterranean and western Asia, particularly in Hellenistic and Roman times. Much of her research to date has revolved around themes of landscape, imperialism, sacred space, and memory. Her fieldwork has, until recently, taken the form of regional investigations in Greece and Armenia, but she is now co-director of the Brown University Petra Archaeological Project - (BUPAP) - in southern Jordan.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/salcock">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Armenia; Greece; Jordan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Archaeology and the Ancient World; Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher egy africa egyptologyandancientwesternasianstudies humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Allen, James</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Allen’s current research focuses on ancient Egyptian language and the models with which it is studied. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jpallen">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Egypt</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pr1 europe portugueseandbrazilianstudies humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Almeida, Onesimo</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Almeida's teaching and research interests include: the intellectual and cultural history of Portugal; Portuguese national identity; science in Portugal in the 15th and 16th centuries; Azorean literature and culture; Portuguese-American literature and studies; ideology and worldviews; and philosophy and social sciences.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/oalmeida">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Portugal</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics languagestudies humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Amanatidou, Elissavet</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Amanatidou is primarily interested in FLLT Methodology, testing and evaluation and the role of literature and cultural enquiry in the development of linguistic and cultural competence.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eamanati">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics; Language Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica  lifesciences" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">AnderBois, Scott</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>AnderBois’s research explores issues in semantics, pragmatics, and their interfaces through a variety of methods including primary fieldwork on Yucatec Maya (an indigenous language of Mexico). He also focuses largely on exploring the various ways in which speakers raise and resolve issues, and keep track of information and its sources both within sentences and in discourse.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sanderbo">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Anderson, Amanda</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Anderson's research focuses on 19th- and 20th-century literature and culture, addressing broad questions of intellectual history, disciplinary formation, and the relation of art and politics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/asa1">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa obstetricsandgynecology biology&medicine" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Anderson, Brenna</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Anderson's research interests include adverse effects of sexually transmitted infections on pregnancy outcomes. She is also interested in reproductive infectious diseases and immunology specifically as they relate to pregnancy outcomes.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bla">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Obstetrics and Gynecology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tha grl asia northamerica anthropology socialsciences" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Anderson, Douglas</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Anderson's research interests include the archaeology and ethnology of northern peoples, with multiple field projects in northwestern Alaska, and the early archaeology of Southeast Asia, with field projects in cave and rock shelter sites along the Andaman Sea coast of Thailand.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ddanders">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greenland; Thailand</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bih mex europe northamerica internationalstudies politicalscience socialsciences" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Andreas, Peter</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Andreas has published nine books. This includes, Blue Helmets and Black Markets: The Business of Survival in the Siege of Sarajevo (Cornell University Press, 2008); Policing the Globe: Criminalization and Crime Control in International Relations (co-author, Oxford University Press, 2006); and, Border Games: Policing the U.S.-Mexico Divide (Cornell University Press, 2nd edition 2009). His latest book is on the politics of smuggling in American history, titled, Smuggler Nation: How Illicit Trade Made America (Oxford University Press, 2013).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pandreas">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Bosnia and Herz.; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>International Studies; Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr irl europe english humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Armstrong, Paul</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Armstrong’s ongoing research is on neuroaesthetics; his work-in-progress includes projects on impressionism, Bloomsbury, and the politics of modernism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pbarmstr">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Ireland; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tha asia medicine biology&medicine" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Artenstein, Andrew</h4>
		<p class="researcher-title">Adjunct Professor</p>
		<div class="researcher-description"><p>Artenstein’s research interests are in the areas of anthrax toxins and pathogenesis, biological warfare, civilian biological defense and emerging pathogens.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/aartenst">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Thailand</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr psx fra asia europe comparativeliterature moderncultureandmedia humanities" data-lastname="A">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Azoulay, Ariella</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Azoulay researches revolutions (18th century to the present); she has also done extensive research on the Israeli-Palestinian conflict.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/aazoulay">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Israel; Palestine</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Modern Culture and Media</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mli africa theatreartsandperformancestudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bach-Coulibaly, Michelle</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Bach-Coulibaly is a performance artist and educator who, since coming to Brown in 1987, has created over 40 original pieces of contemporary movement theatre for the concert stage that investigate socio-political canvases, cross-cultural narratives, and embodied texts. Since 1990, her work in Mali has been focused upon building Yeredon, a research center for cultural preservation, international collaboration and social activism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mbachcou">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mali</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Theatre Arts and Performance Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe engineering physicalsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bahar, Ruth</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bahar is currently working to improve reliability and reduce dissipation in high-performance processors.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rbahar">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu gbr europe mathematics physicalsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Banchoff, Thomas</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Banchoff's research areas include the geometry and topology of smooth and polyhedral surfaces in three- and four-dimensional space, as well as development and dissemination of Internet-based courseware for communication and visualization in undergraduate mathematics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tbanchof">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ukr europe history socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bartov, Omer</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bartov’s research aims to trace the origins of local mass murder in the complexities of relations between different ethnic and religious groups over a long time span in the Eastern Galician town of Buczacz.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/obartov">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Ukraine</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bass, Laura</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Bass is currently researching and writing a cultural history of Madrid in the period of its consolidation as the court capital of imperial Spain. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lrbass">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe neuroscience biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bath, Kevin</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Bath’s research is heavily focused on understanding emotional development and how environmental or pharmacological stressors incurred early in life alter the developmental trajectory of brain centers involved in cognitive and emotional regulation.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kbath">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Neuroscience</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bauer, Beth</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Bauer’s current research examines issues of gender and empire in the travel books of Fanny Calderón de la Barca.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bbauer">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher cri asm northamerica oceania epidemiology biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Baylin, Ana</h4>
		<p class="researcher-title">Adjunct Assistant Professor</p>
		<div class="researcher-description"><p>Baylin is primarily interested in the emerging burden of chronic disease in developing countries, in particular cardiovascular disease, obesity, and diabetes, which have started to exceed those in developed countries. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/abaylin">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>American Samoa; Costa Rica</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher arm rus kgz nic khm ken asia europe northamerica africa behavioralandsocialsciences emergencymedicine biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Becker, Bruce</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Becker is the principal investigator on a Robert Wood Johnson Foundation grant "Smoking Cessation in Mothers and Older Family Members of Babies Being Treated in a Special Care Nursery." He has worked in many countries doing refugee medicine, disaster medicine and emergency medicine.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bbeckerm">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Armenia; Cambodia; Kenya; Kyrgyzstan; Nicaragua; Russia</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences; Emergency Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dza mar tun fra africa europe comparativeliterature frenchstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bensmaia, Reda</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bensmaïa’s current research focuses on the evolution of democratic institutions in Algeria, Morocco and Tunisia as reflected in the works of Francophone writers and intellectuals during the last fifteen years.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rbensmai">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Algeria; France; Morocco; Tunisia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nga sle moz africa anthropology socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Benton, Adia</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Benton is a medical anthropologist with interests in science and technology studies, and in political and legal anthropology in sub-Saharan Africa. Her research in medical anthropology includes work on HIV/AIDS, essential surgical care, race and DNA ancestry, post-conflict development, humanitarianism and gender violence.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/abenton">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Mozambique; Nigeria; Sierra Leone</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica engineering physicalsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Beresford, J Roderic</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Beresford researches semiconductor nanostructures.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jberesfo">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia coguthumanitiescenter internationalstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Berman, Nathaniel</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Berman is interested in international law, religion, and colonial and nationalist discourses.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nb11">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Cogut Humanities Center; International Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu fra europe comparativeliterature germanstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bernstein, Susan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bernstein works in German, French and English and American literature of the 18th-20th centuries. She has particular interests in literary theory, literature and the arts, Romanticism, philosophy and poetry.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sbernste">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chl southamerica ecologyandevolutionarybiology biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bertness, Mark</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bertness’s research is focused on the dynamics of natural communities, marine shorelines systems and conservation biology.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mbertnes">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Chile</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher sdn egy aut africa europe archaeologyandtheancientworld egyptologyandancientwesternasianstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bestock, Laurel</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Bestock's research focuses on the material culture of the Nile Valley. She is interested in kingship and monumentality, but also in the development of sacred space over time and on cultural interactions.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lbestock">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Austria; Egypt; Sudan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Archaeology and the Ancient World; Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr zaf europe africa english moderncultureandmedia humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bewes, Timothy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bewes has research interests in contemporary British/American fiction, aesthetic theory, poststructuralist and Marxist literary theory, postmodernism and postcolonialism, and the politics and ethics of literary form.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tbewes">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>South Africa; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English; Modern Culture and Media</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica surgery biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bianchi, Cesario</h4>
		<p class="researcher-title">Associate Professor Research</p>
		<div class="researcher-description"><p>Bianchi has been studying the effects of drugs and supplements in hypercholesterolemic and diabetic pigs subjected to chronic and acute models of myocardial ischemia. In 2008 he became the US organizer of a short-term wet bench annual training program for FCMSCSP (Brazil) medical students.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cbianchi">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Surgery</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia historyofartandarchitecture " data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bickford, Maggie</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bickford engages in object-centered research in the history of painting in China during the Song and Yuan periods. Her work ranges from genre-formation in scholar-amateur ink painting to production and reception of imperial art.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rbickfor">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica education socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bisaccio, Daniel</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Bisaccio’s current research project concerns biodiversity in central and South American and the South Pacific sites.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dbisacci">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Education</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tur asia english humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Blasing, Mutlu</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Blasing researches American poetry. She has also translated from Turkish into English.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mblasing">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher arg southamerica economics socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Blaum, Joaquin</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Blaum is interested in international trade and macroeconomics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jblaum">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Argentina</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe politicalscience socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Blyth, Mark</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Blyth’s research interests lie in the of field of international political economy. More specifically, his research traverses several fields and aims to be as interdisciplinary as possible, drawing from political science, economics, sociology, complexity theory and evolutionary theory.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mblyth">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra deu gbr europe classics humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bodel, John</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bodel studies ancient Roman social, economic, and cultural history and Latin literature, especially of the empire. Much of his research involves inscriptions, and he has special interests in Roman religion, slavery, funerals and burial customs, ancient writing systems, the editing of Latin epigraphic and literary texts, and Latin prose authors.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jbodel">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hti northamerica africanastudies socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bogues, Barrymore</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bogues's research and writing interests are intellectual, literary and cultural history, radical political thought, political theory, critical theory as well as Caribbean and African politics. He is currently working on Human Freedom, an edited Reader on Black Political Thought, and a series of projects on Haitian art.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bbogues">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Haiti</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Africana Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe historyofartandarchitecture humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bonde, Sheila</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bonde's research brings together the approaches of archaeology, history, architectural history and spatial analysis. She is co-director of the MonArch (Monastic Archaeology) research team that focuses on three monasteries in northern France.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sbonde">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hnd isr ton northamerica asia oceania familymedicine biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Borkan, Jeffrey</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Borkan has research interests in qualitative research and mixed qualitative-quantitative studies; doctor-patient communication and narratives; low-back pain in primary care; public participation in health policy decisions, such as rationing; and medical education.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jborkanm">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Honduras; Israel; Tonga</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe comparativeliterature frenchstudies humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bossy, Michel-Andre</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bossy studies medieval cultural connections between France and its neighbors, especially during the period of the troubadours and the Hundred Years' War. His fields include medieval French, Anglo-Norman, and Occitan literature, 12th- to 15th-century lyric poetry, and social interpretations of literature</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mbossy">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher asm oceania emergencymedicine biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bouslough, David</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>Bouslough works in International Emergency Preparedness, Palliative Care, Disaster Medicine and Response.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dbouslou">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>American Samoa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Emergency Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe  biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bowen, Wayne</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Bowen's research aims to understand the underlying mechanisms of programmed cell death (Apoptosis) caused by activation of sigma-2 receptors. Because these receptors are highly expressed in cancer cells, they can be targeted for development of new antineoplastic agents (anti-cancer drugs).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wbowen">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa africanastudies pathologyandlaboratorymedicine biology&medicine" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Braun, Lundy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Braun focuses on the history of race in public health, medicine, and technology. Current particular interests center on how biological understandings of race obscure the structural causes of health inequality.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lbraun">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Africana Studies; Pathology and Laboratory Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can rus isr northamerica europe asia mathematics physicalsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Braverman, Alexander</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Braverman’s research focuses on the geometric Langlands program. A relatively new field which grew out of an attempt to understand some phenomena of number theory predicted by Langlands, this subject lies on the border of such fields of mathematics as number theory, representation theory and algebraic geometry.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/abraverm">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Canada; Israel; Russia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher aus oceania ecologyandevolutionarybiology engineering physicalsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Breuer, Kenneth</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Breuer’s research focuses on fluid mechanics and animal flight. He was a visiting scholar at the University of Queensland.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kbreuer">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Australia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology; Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra gbr europe philosophy humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Broackes, Justin</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Broackes is interested in the philosophy of mind and language, Descartes, Locke and Hume. His present research is on issues in metaphysics and the theory of perception, and their connections with the history of the subject. Special areas of interest include: Theory of Color and Color-Perception, from the Ancient Greeks to the present; Color-Blindness; and the Notion of Substance, and what became of that idea in the 17th and 18th centuries and after. In addition, Broackes is working on a book on Iris Murdoch's The Sovereignty of Good and is editing a collection of essays on her work.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jbroacke">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia history socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Brokaw, Cynthia</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Brokaw researches the history of the book in late imperial China. Her current project examines the development of publishing and the creation of book cultures, both Chinese and Tibetan, on the southwestern frontier of the Qing Empire.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cbrokaw">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mkd europe internationalstudies socialsciences" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Brown, Keith</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Brown works primarily in the domain of culture, politics and identity. As well as extensive research on ethno-nationalism and the role of national history in the Balkans, his more recent work explores how different transnational processes contribute to people's sense of long-distance connection, and new forms of citizenship and belonging. He has served since July 2010 as Director of the Brown International Advanced Research Institutes (BIARI).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kbrown2">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Macedonia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>International Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Bryan, Elizabeth</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Bryan researches medieval Brut Chronicle narratives and their evolving interpretations, medieval and early modern palaeography and codicology, theories of authorship and textual production in manuscript cultures, and Early Middle English vernacularity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ebryan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia classics humanities" data-lastname="B">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Buchta, David</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Bucht is a Sanskrit scholar and expert in classical Indian literary theory, Indian philosophy, and ancient and medieval Hinduism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dbuchta">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp gbr europe engineering physicalsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Calo, Joseph</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Calo’s work applies themes of chemical kinetics and transport phenomena to problems in engineering and science.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jcalo">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Spain; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jor mar tun lbn asia africa politicalscience socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cammett, Melani</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Cammett's research interests include comparative politics and political economy (development, social welfare, institutional change, business-government relations), public health, methodology (research design and qualitative methods), and Middle Eastern and North African politics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mcammett">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Jordan; Lebanon; Morocco; Tunisia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn jpn fra asia europe chemistry physicalsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cane, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cane's research seeks to establish the mechanism of formation of a wide variety of naturally occurring substances of diverse biological origins including antibiotics, toxins, essential oils, and vitamins. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dcane">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; France; Japan</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe slaviclanguages humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Carey, Claude</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Carey has research interests in 19th- and 20th-century Russian Literature, and Russian Language. Her current research interests are in Orientalism in Russian and Soviet literature. She is pursuing research on Russian and Soviet writers' perception of the East.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ccarey">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa behavioralandsocialsciences socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Carey, Kate</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Carey studies the causes and consequences of risky drinking, and the factors contributing to alcohol-related risk reduction. Research interests include screening and brief interventions for at-risk drinking, motivation to change, young adult drinking, alcohol use and risky sexual behavior.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kc11">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia medicine biology&medicine" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Carpenter, Charles</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Carpenter is the Director of the Brown Center for AIDS research. His research over the past decade has been directed toward two main areas, the optimal treatment of HIV infection in North American women and therapeutic strategies that are effective in the developing world.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ccarpent">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa medicine biology&medicine" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Carter, E</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Carter’s interest focuses on Tuberculosis Program Development and Care Delivery. TB is both the leading killer from a single infectious agent in the world as well as the leading cause of death in patients living with HIV globally. Carter’s work focuses both locally at the RI TB Clinic and internationally (primarily in Kenya) to develop community based care programs, promote DOTS expansion, new TB diagnostics for the developing world and coordinated care progams for TB/HIV. Coordinator of the Brown-Kenya Moi Exchange Program (<a href="http://bms.brown.edu/students/exchange.php" title="http://bms.brown.edu/students/exchange.php">http://bms.brown.edu/students/exchange.php</a>).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ejcarter">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher sen africa anthropology urbanstudies socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Carter, Rebecca</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Carter's research explores human dwelling, vulnerability, and social-environmental change, particularly in the urban periphery.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rcarter1">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Senegal</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology; Urban Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe italianstudies humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Castiglione, Caroline</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Castiglione examines how seemingly marginalized individuals challenged systems of power in Italy during the period 1500-1800. Her research also investigates the intersection of mothering and politics in 17th-century Rome.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ccastigl">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp phl europe asia hispanicstudies humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Chang, Julia</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Chang’s research and teaching fields include 19th-century Spanish literature and culture with a special focus on the realist novel, gender studies, medical hygiene, and Spanish colonialism. More recently, she has begun researching Hispano-Asian relations in the 19th century, examining fictional and ethnographic writing on the Philippines.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jc231">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Philippines; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia eastasianstudies humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Chen, Wenhui</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Chen teaches Modern Chinese at Brown.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wc53">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr alb ita arm grc msr europe asia northamerica archaeologyandtheancientworld classics humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cherry, John</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cherry's research investigates changes in material culture on Crete in the 3rd and 2nd millennium that signal the rise of sociopolitical complexity and the earliest states on the island. He currently co-directs a diachronic field survey project on Montserrat in the Caribbean Lesser Antilles.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jfcherry">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Albania; Armenia; Greece; Italy; Montserrat; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Archaeology and the Ancient World; Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu gbr fra ken uga tza europe africa sociology socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Chorev, Nitsan</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Chorev's research focuses on the politics of neoliberalism and globalization. She looks at the policies promoted by the World Health Organization (WHO) during the call for a New International Economic Order in the 1970s and the under neoliberalism in the 1990s to investigate how international organizations respond to exogenous pressures.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nchorev">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany; Kenya; Tanzania; Uganda; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher egy africa languagestudies humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Christoff, Mirena</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Christoff’s research topics include modern Arabic translation and cultural history; the study of aspects of the linguistic transmission between non-cognate languages; the integration of translation in foreign language instruction.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mchristo">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Egypt</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Language Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe moderncultureandmedia humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Chun, Wendy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Chun has studied both Systems Design Engineering and English Literature, which she combines and mutates in her current work on digital media. She is currently a Visiting Professor at Leuphana University (Luneburg, Germany).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wchun">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Modern Culture and Media</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa surgery biology&medicine" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cioffi Jr, William</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cioffi Jr. is a national leader in trauma, critical care, and burns. Throughout his career his major research interests have focused on the host response to injury and include basic science, translational, and clinical research. He has also participated in the esophageal cancer program at Tenwek Hospital in Kenya.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wcioffij">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Surgery</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe religiousstudies humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cladis, Mark</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cladis’s research focuses on the history of Western political, social, and religious thought, especially the religious nature and origins of liberal, democratic society. Religion and the environment is another subject of his research and teaching. He has written extensively on the work of Emile Durkheim.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mcladis">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher per southamerica comparativeliterature hispanicstudies humanities" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Clayton, Michelle</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Clayton’s research spans modern and contemporary Latin American and European writing and film, avant-garde aesthetics and poetry, dance, art history, and media studies. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mc133">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Peru</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind chn vnm mys brn phl asia geologicalsciences physicalsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Clemens, Steven</h4>
		<p class="researcher-title">Associate Professor Research</p>
		<div class="researcher-description"><p>Clemens uses the marine and terrestrial geologic record to understand the history and dynamics of global climate and environmental change, with an emphasis on the Indian and Asian monsoon systems.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sclemens">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brunei; China; India; Malaysia; Philippines; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tun dza africa history socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Conant, Jonathan</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Conant studies late ancient and early medieval history. His research focuses on the inter-regional integration of the Mediterranean, and he has a special interest in questions of identity, empire, interfaith interaction, sanctity, slavery, and documentary culture.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jconant">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Algeria; Tunisia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia neuroscience biology&medicine" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Connors, Barry</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Connor’s research studies the properties of the neurons of the neocortex of the brain, their synaptic connections, and the characteristics of cortical networks. The neocortex is responsible for thinking, remembering, processing sensory information, and controlling movement. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bconnors">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Neuroscience</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nld gbr europe history socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cook, Harold</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cook has helped to pioneer the use of the medical marketplace as a historical tool of investigation, and more recently, through examinations of the Dutch experience, has been especially interested in the connections between the rise of global commerce and the development of a global science.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hjcook">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Netherlands; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe politicalscience socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cook, Linda</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cook's main research interests are in the comparative politics of the Russian Federation, Eastern Europe and Eurasia, and electoral-authoritarian regimes cross-regionally. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lcookphd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita isr europe asia physics physicalsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cooper, Leon</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cooper studies neural networks, including architecture, learning rules, and real world applications; the biological basis of memory and learning; mean field theories; the foundations of quantum theory; and superconductivity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lcooper">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Israel; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica history socialsciences" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cope, Robert</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Cope's research and teaching focuses on the creation and development of multi-ethnic societies in Mexico and Central America. Cope is particularly interested in the lived experience of the urban poor: how they grappled – socially, economically, and culturally – with their unfavorable position in the colonial hierarchy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rcope">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe comparativeliterature germanstudies " data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Crossgrove, William</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Crossgrove works on "knowledge" literature in late medieval Germany and on the process of "vernacularization" whereby Latin scientific texts and unwritten artisanal skills are transmuted into vernacular texts for a newly-literate reading public of non-experts.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wcrossgr">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher khm ken phl ind idn asia africa healthservicespolicyandpractice obstetricsandgynecology biology&medicine" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cu-Uvin, Susan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Cu-Uvin is interested in STDs, AIDS, HPV, and HIV transmission prevention.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/suvinmd">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cambodia; India; Indonesia; Kenya; Philippines</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Health Services Policy and Practice; Obstetrics and Gynecology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher vnm asia familymedicine biology&medicine" data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Cummings, Stephen</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>Cummings is involved in the mission of the Vietnam Family Medicine Development Project, whose goal is to introduce and establish family medicine as the core specialty for primary care throughout Vietnam.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/scumming">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe chemistry " data-lastname="C">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Curci, Ruggero</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Curci's research investigates the properties and the reactivity of new peroxide species which allow selective oxidations. Remove: These species have great potential for synthetic purposes. Add: His research has been supported by the Italian Research Council, the Italian Chemical Society, and the Italian Ministry of Education among others. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rcurci">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe slaviclanguages humanities" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">DeBenedette, Lynne</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>DeBenedette teaches Russian language and coordinates the Russian language curriculum. In alternate summers she serves as the coordinator of Brown in St. Petersburg, and is the advisor for study abroad in Russia. Her research interests include curriculum design, instructional technology, Processing Instruction and foreign language teacher education.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ldebened">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita grc europe classics humanities" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Debrohun, Jeri</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>DeBrohun's primary research interests are in Republican and Augustan Latin poetry and culture, with particular emphasis on allusion and genre.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jdebrohu">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe physics physicalsciences" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Dell'Antonio, Ian</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Dell’Antonio’s research centers on observational cosmology, the experimental measurement of the fundamental properties of the Universe.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/idellant">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher asm oceania psychiatryandhumanbehavior biology&medicine" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">DePue, Judith</h4>
		<p class="researcher-title">Clinical Professor</p>
		<div class="researcher-description"><p>DePue's research interests focus on public health approaches to deliver preventive care and behavioral/lifestyle interventions. She has particular interest in developing interventions for primary care and community settings and with under-served populations. She has recently completed a randomized clinical trial on cultural translation of a diabetes self-management intervention.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jdepueed">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>American Samoa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher egy africa egyptologyandancientwesternasianstudies humanities" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Depuydt, Leo</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Depuydt’s most recent research efforts involved ancient science and math in Egypt, with a focus on probability theory.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ldepuydt">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Egypt</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dom northamerica medicine biology&medicine" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Diaz, Joseph</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Diaz researches cancer control and prevention, colon cancer, and language barriers to health care access.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jdiazmd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Dominican Rep.</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hrv europe sociology socialsciences" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Dill, Ann</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Dill studies issues faced by nonprofit/non-governmental organizations, including leadership dynamics, relationships with the state, and management and governance challenges. Her current research looks at how NGOs respond to growing inequalities in access to health care in post-communist settings, in particular Croatia.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/adill">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Croatia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita deu europe  lifesciences" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Domini, Fulvio</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Domini is interested in how human survival capabilities depend on a fundamental skill of the visual system: interpreting retinal images in order to extract knowledge about the 3D structure of the surrounding environment. Domini investigates what kind of mathematical analysis of different properties of retinal images can be considered biologically plausible</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/fdomini">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor asia appliedmathematics physicalsciences" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Dong, Hongjie</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Dong's research interest is in partial differential equations, including nonlinear elliptic and parabolic equations, the Navier-Stokes equations, the quasi-geostrophic equations, the reaction diffusion equations, the probability approaches of PDEs, and rates of convergence of finite difference approximations for elliptic and parabolic Bellman's equations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hdong">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Korea</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Applied Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher psx syr asia history middleeaststudies socialsciences" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Doumani, Beshara</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Doumani's research focuses on the history of social groups, places, and time periods that have been silenced or erased by conventional scholarship on the early modern and modern Middle East. He helped pioneer the fields of Middle East family history and the social history of the Palestinians.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bdoumani">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Palestine; Syria</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History; Middle East Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe ecologyandevolutionarybiology biology&medicine" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Dunn, Casey</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Dunn investigates how evolution has produced a diversity of life. We primarily focus on form (i.e. morphology), and are interested in learning about both the actual history of life on Earth and general properties of evolution that have contributed to these historical patterns. The type of questions the Dunn Lab asks require field (mostly marine), laboratory, and computational work. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cd7">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia medicine biology&medicine" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Dworkin, Lance</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Dworkin researches kidney disease, hemodynamics, and hypertension.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ldworkin">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra gha southamerica africa africanastudies portugueseandbrazilianstudies socialsciences" data-lastname="D">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Dzidzienyo, Anani</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Dzidzienyo specializes in the study of race in Latin America and the encounters between Latin Americans and Africans, as well as on the life and work of George Padmore and Kwame Nkrumah.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/adzidzie">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Ghana</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Africana Studies; Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe economics socialsciences" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Eaton, Jonathan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Eaton’s work is interdisciplinary. He has researched Latin American countries receiving loans from US commercial banks. He was a visiting scholar at the Centre de Recherche en Économie et Statistique.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jweaton">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ven dom southamerica northamerica ecologyandevolutionarybiology biology&medicine" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Edwards, Erika</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Edwards is a botanist interested in plant evolution.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ejedward">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Dominican Rep.; Venezuela</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Egan, James</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Egan’s work includes the following: 17th- and 18th-century British-American writing; the history of the book; the early modern Atlantic world; colonial British-American Orientalism; and early theories of modernity. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jegan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rwa uga africa theatreartsandperformancestudies humanities" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ehn, Erik</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Ehn conducts annual trips to Rwanda/Uganda, bringing teams to study the history there, and explore the ways art is participating in recovery from violence.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eehn">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Rwanda; Uganda</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Theatre Arts and Performance Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica internationalstudies socialsciences" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Elliott, Claudia</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Elliott’s work and research focus on Latin American and comparative politics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/celliott">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>International Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dom grc deu northamerica europe familymedicine biology&medicine" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Empkie, Thimothy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Empkie is responsible for a series of initiatives and programs of an internationally collaborative nature.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tempkiem">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Dominican Rep.; Germany; Greece</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher aus oceania philosophy humanities" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Estlund, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Estlund's areas of expertise include ethics and social/political philosophy, particularly the areas of liberalism, justice, and democracy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/destlund">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Australia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe comparativeliterature slaviclanguages humanities" data-lastname="E">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Evdokimova, Svetlana</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Evdokimova's main areas of scholarly interest include Pushkin, Russian and European Romanticism, Tolstoy, Chekhov, relations between fiction and history, aesthetics, and gender and sexuality in Russian and European literatures.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sevdokim">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dom northamerica medicine biology&medicine" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Fagan, Mark</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Fagan's academic interests are in internal medicine education. He is co-course leader (with Joe Diaz) of Internal Medicine in the Dominican Republic. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mfaganmd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Dominican Rep.</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tun africa languagestudies humanities" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Faiza, Miled</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Faiza is primarily interested in Modern Arabic Literature, Translation and Lexicography.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mfaiza">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Tunisia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Language Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica anthropology socialsciences" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Faudree, Paja</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Faudree's research interests include language and politics, indigenous literary and social movements, the interface between music and language, the ethnohistory of New World colonization, and the global marketing of indigenous rights discourses, indigenous knowledge, and plants.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pfaudree">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia healthservicespolicyandpractice " data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Feng, Zhanlian</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Feng’s recent research concerns the emergence of institutional health care in China. His team aggregates data from a sample of elder care homes in Tianjin and Nanjing to understand the needs of the fast aging population in China.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/zfeng">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Health Services Policy and Practice</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra ago southamerica africa history portugueseandbrazilianstudies socialsciences" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ferreira, Roquinaldo</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Ferreira specializes in African, Colonial Brazilian, and Atlantic Histories. He is also interested in comparative slavery and forced labor, race relations in colonial societies, production of racial ideologies, and abolitionism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rf25">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Angola; Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History; Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher cze rus jpn europe asia slaviclanguages humanities" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Fidler, Masako</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Fidler studies discourse analysis and cognitive linguistics with an emphasis on Czech, Japanese, and Russian. She is particularly interested in the connection between sound and meaning in language and how this relationship interacts with grammar. Her monograph on Czech onomaopoeia came out in 2014.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mufidler">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Czech Rep.; Japan; Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr can europe northamerica visualart humanities" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Fishman, Richard</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Fishman is an award-winning sculptor whose work has been exhibited throughout the United States, as well as in London and Montreal.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rfishman">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Canada; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Visual Art</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia classics humanities" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Fitzgerald, James</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Fitzgerald is a scholar of Sanskrit and ancient Indian literature and intellectual history.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jlf2">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gha cpv ukr ind vnm africa europe asia healthservicespolicyandpractice " data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Flanigan, Timothy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Flanigan is currently involved in clinical research programs for HIV care, treatment, and testing in underserved communities.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tflaniga">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cape Verde; Ghana; India; Ukraine; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Health Services Policy and Practice</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe comparativeliterature english humanities" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Foley, Stephen</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Foley works on European Renaissance culture and letters, classical traditions, lyric poetry, religion and literature, literary theory, and aesthetics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sfoley">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia economics socialsciences" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Foster, Andrew</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Foster studies household and family economics, health economics, and economic development. His recent research focuses on economic growth in rural India and air quality in Delhi.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/afoster">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe populationstudies socialsciences" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Franklin, Rachel</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Franklin is a population geographer and regional scientist whose research focuses on migration and population change. Her current work focuses on the grography of population decline in the U.S. and Germany, as well as the migration patterns of U.S. college students.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rsfrankl">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Population Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia economics socialsciences" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Friedberg, Rachel</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Friedberg's main area of expertise is labor markets and immigration. Her research is focused on labor market performance and immigration considering factors like education, work experience, age at arrival, and language in the US and Israel.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rfriedbe">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher phl gbr nld bra asia europe southamerica pediatrics biology&medicine" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Friedman, Jennifer</h4>
		<p class="researcher-title">Associate Professor Research</p>
		<div class="researcher-description"><p>Friedman’s research focuses on how schistosomiasis and malaria cause morbidity for children and pregnant women.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jffriedm">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Netherlands; Philippines; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind eri asia africa anthropology socialsciences" data-lastname="F">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Fruzzetti, Lina</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Fruzzetti's research focuses on social anthropology, kinship, politics, study of ritual and the construction of gender, development and political studies, race and ethnic relations, Islamic societies and notions of identity, ethnographic film; feminist movement in Africa and Asia, study of ritual and kinship, construction of gender and identity, nationalism and post-colonial identity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lfruzzet">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Eritrea; India</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe physics physicalsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gaitskell, Richard</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gaitskell leads a research team hunting for direct evidence of particle dark matter, one of physics' greatest unclaimed prizes.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rgaitske">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex ken northamerica africa  biology&medicine" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Galarraga, Omar</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Galarraga’s research focuses on applied health economics and health and services research. This includes understanding economic incentives to prevent HIV, and the economic efficacy of HIV programs.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ogalarra">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Kenya; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita deu europe economics socialsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Galor, Oded</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Galor focuses his research in the fields of income distribution, human evolution, population economics, and economic growth models.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ogalor">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe comparativeliterature literaryarts humanities" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gander, Forrest</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gander is concerned with the way identity is translated by encounters with the foreign. The editor of several anthologies of poetry from Spain and Latin America, Gander is also a well-known translator.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/fgander">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Literary Arts</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor sgp chn asia engineering physicalsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gao, Huajian</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gao’s research focuses on applied mechanics and engineering science. His work has been funded by a number of foreign governments, and he has worked as visiting professor abroad.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hgao">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Korea; Singapore</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe ecologyandevolutionarybiology biology&medicine" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gatesy, Stephen</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gatesy’s research is directed at understanding the evolution of the vertebrate locomotor system with a focus on birds, dinosaurs and other extinct species. Gatesy uses computer models and 3-d motion analysis in his studies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sgatesy">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa  biology&medicine" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Genberg, Becky</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Genberg’s current research focuses on HIV care and home-based counseling and testing in Kenya.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bgenberg">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics philosophy humanities" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gill, Mary Louise</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gill focuses on the field of ancient Greek philosophy, and specializes on metaphysics, epistemology, method, and natural philosophy of Plato and Aristotle.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mggill">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics; Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe history judaicstudies,comparativeliterature socialsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gluck, Mary</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gluck’s latest book focuses on Popular Bohemia: Modernism and Urban Culture in 19th-Century Paris. Currently, she is working on Jewish humor and assimilation in fin-de-siècle Budapest and on bohemias in a global perspective.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mgluck">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History; Judaic Studies, Comparative Literature</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher asm oceania psychiatryandhumanbehavior biology&medicine" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Goldstein, Michael</h4>
		<p class="researcher-title">Adjunct Professor</p>
		<div class="researcher-description"><p>Goldstein researches smoking cessation and preventive interventions; he also studies strategies to enhance physical activity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mggoldst">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>American Samoa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe slaviclanguages humanities" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Golstein, Vladimir</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Golstein's scholarly interests embrace Russian culture, religion, philosophy, and poetry, of the past two centuries.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/vgolstei">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe comparativeliterature theatreartsandperformancestudies humanities" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Golub, Spencer</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Golub’s current research, which is often a hybrid of different fields, overlaps the disciplines of theatre studies and performance studies, incorporating investigations into cultural performance and event (especially, but not exclusively, in Russian/Soviet contexts) and mental performance and event.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sgolub">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Theatre Arts and Performance Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe music humanities" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gooley, Dana</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Gooley’s main research centers on European music and musical culture in the 19th century. In the past he has researched in Berlin.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dgooley">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica history portugueseandbrazilianstudies socialsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Green, James</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Green works on the political, social and, and cultural history of 19th- and 20th-century Brazil.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jngreen">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History; Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe history socialsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Guldi, E Joanna</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Guldi specializes in the history of capitalism, land use, and the design of computational tools for visualizing large numbers of texts.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eguldi">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div>-- end researcher-side -->
	
</li>
    

<li class="researcher hti cub northamerica africanastudies americanstudies socialsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Guterl, Matthew</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Guterl studies the American histories of "race" and "nation" in an international context. He considers how these concepts have evolved over time, how they relate to each other at important moments, how certain social forces that have shaped them, and how ordinary and extraordinary people are caught up in their histories. He has written four books on topics as different as the Great Migration, the Old South, racial profiling, and Josephine Baker's adopted family.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mguterl">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cuba; Haiti</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Africana Studies; American Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex hti lbn northamerica asia anthropology socialsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Gutmann, Matthew</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Gutmann's research and teaching focuses on democracy and social change; poverty, inequality, and development; health; gender; and militarization.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mgutmann">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Haiti; Lebanon; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe philosophy humanities" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Guyer, Paul</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Guyer is a renowned scholar of German Idealism, especially Kant’s philosophy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pguyer">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chl ind esp southamerica asia europe appliedmathematics physicalsciences" data-lastname="G">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Guzman, Johnny</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Guzman's main research interest lies in the area of numerical approximations to partial differential equations (PDEs).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jg26">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Chile; India; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Applied Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe pathologyandlaboratorymedicine biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Haddow, James</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Haddow’s applied research focuses on screening, with a most recent emphasis on prenatal screening for fetal and maternal disorders.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jhaddow">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pathology and Laboratory Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher egy africa anthropology socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hamdy, Sherine</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Hamdy's researchinterests include cross-cultural approaches to medicine, health, and the body.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/shamdy">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Egypt</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ner africa obstetricsandgynecology biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hampton, Brittany</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Hampton takes a senior fellow with her annually in April for an outreach trip to Niger, West Africa. The objectives of the trip are to give women’s health aid where needed, create opportunities for international medical experiences for physicians-in-training, provide exposure to complicated/advanced problems in reconstructive pelvic surgery, and to gain experience in working towards a common goal with a multidisciplinary, multinational medical team. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bhampton">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Niger</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Obstetrics and Gynecology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics humanities" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hanink, Johanna</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Hanink is especially interested in Greek theatrical production as well as in performance in (and of) classical Athens.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jhanink">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tur irq syr asia archaeologyandtheancientworld egyptologyandancientwesternasianstudies humanities" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Harmansah, Omur</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Harmansah works in the field of archaeology, architectural history and material culture of the Near East, with emphasis on Anatolia, Syria, and Mesopotamia.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/oharmans">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Iraq; Syria; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Archaeology and the Ancient World; Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr irl europe history socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Harris, Timothy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Harris is interested in the interaction between high politics and low politics and the study of popular protest, popular culture, religion, and politics in England, Scotland, and Ireland during Britain's Century of Revolutions.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tgharris">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Ireland; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa behavioralandsocialsciences populationstudies socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Harrison, Abigail</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Harrison's research addresses social and contextual influences on young women's reproductive decision-making in relation to HIV prevention and transmission in southern sub-Saharan Africa.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/aharriso">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences; Population Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hnd northamerica familymedicine biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Harrison, Emily</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>Harrison and a team of Brown doctors opened a new clinic in rural Honduras in the summer of 2011. The facility, operated by a nongovernmental organization that Harrison directs called Shoulder to Shoulder, brings new hope to thousands in the forms of medicine and programs to improve nutrition and family planning. She is interested in Women's Health and Obstetrics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/emharris">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Honduras</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher syr asia religiousstudies humanities" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Harvey, Susan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Harvey is presently working on biblical women in Syriac hymnography and homiletics, and the Syriac women's choirs that performed these works.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/suharvey">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Syria</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher khm chn idn pn1 vnm asia oceania pediatrics biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Harwell, Joseph</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Harwell’s research focuses on HIV prevention, particularly among women and with mother-to-child transmission.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jharwell">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cambodia; China; Indonesia; Papua New Guinea; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grl northamerica geologicalsciences physicalsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hastings, Meredith</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Hasting's research focuses on the reactive nitrogen cycle, with an emphasis on nitrate deposition.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mhasting">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greenland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra gbr europe americanstudies socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Haviland, Beverly</h4>
		<p class="researcher-title">Senior Lecturer/Visiting Associate Professor</p>
		<div class="researcher-description"><p>Haviland works on 19th-century American, English, and French Literatures, film, and cultural history.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bhavilan">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>American Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu gbr europe classics comparativeliterature humanities" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Haynes, Kenneth</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Haynes studies the classical tradition in European literature and philosophy since the Renaissance, with particular attention to German and British Hellenism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/khaynes">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics; Comparative Literature</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher che europe physics physicalsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Heintz, Ulrich</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Heintz studies the fundamental building blocks of matter. Their interactions are described by the standard model of elementary particle physics. Heintz is interested in testing the validity of this model at very high energies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/uheintz">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Switzerland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra zaf southamerica africa internationalstudies sociology socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Heller, Patrick</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Heller studies how inequality shapes development, urban transformation, democracy and globalization. His work has focused primarily on Brazil, India and South Africa.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/paheller">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>International Studies; Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher atg northamerica africanastudies sociology socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Henry, Paget</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Henry's main area of research is the economic and political problems of the Caribbean. In particular, Currently, Henry is doing some research on the role of culture and the process of development in both the Caribbean and Africa.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/phenry">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Antigua and Barb.</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Africana Studies; Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher per southamerica geologicalsciences physicalsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Herbert, Timothy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Herbert's research focuses on understanding how the earth's climatic system, particularly the ocean, adjusts itself to perturbation on various timescales.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/therbert">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Peru</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr mex gbr asia northamerica europe computerscience physicalsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Herlihy, Maurice</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Herlihy's research interests focus on distributed computing, particularly multiprocessor synchronization and fault-tolerance.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mpherlih">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Israel; Mexico; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Computer Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra dnk europe  " data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hesthaven, Jan</h4>
		<p class="researcher-title">Adjunct Professor</p>
		<div class="researcher-description"><p>Hesthaven's research interests focus on the development, analysis, and application of high-order accurate methods for the solution of partial differential equations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jhesthav">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Denmark; France</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe  biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hoffman-Kim, Diane</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Hoffman-Kim’s research focuses on understanding axon guidance in complex environments and developing biomaterial and tissue engineering strategies for nerve guidance and repair.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dihoffma">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita eth psx europe africa asia sociology socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hogan, Dennis</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Hogan’s research involves the interrelationships of the family lives of individual persons and the influence of social environments (family and community origins, educational opportunities, employment opportunities, cultural definitions of expected roles).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dhogan">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Ethiopia; Italy; Palestine</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can ken arg ind vnm chn northamerica africa southamerica asia biostatistics biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hogan, Joseph</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Hogan's research concerns statistical methods for missing data, causal inference, and sensitivity analysis in HIV/AIDS.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jhogansc">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Argentina; Canada; China; India; Kenya; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Biostatistics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa medicine biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hohenhaus, Mary</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>Hohenhaus assists Jane Carter with the Kenya program. Since 2004, she has been involved in the Kenya exchange program where she rotated at the Moi University Teaching and Research Hospital in Eldoret as a senior resident in internal medicine. In August 2006, she returned as visiting faculty.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mhohenha">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hun tza nga europe africa anthropology socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hollos, Marida</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Hollos studies the population of developing countries, especially fertility, infertility, and the status of women. She is especially interested in how motherhood and the concept of children are configured in different regions.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mhollos">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Hungary; Nigeria; Tanzania</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kos europe obstetricsandgynecology biology&medicine" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hosmer, Jennifer</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>Hosmer spent five days at the University Hospital of Kosovo where they perform approximately 10,000 deliveries a year. There she observed their models of care, education (as it is a teaching hospital as well), and led patient scenario conferences where they discussed the evidence based practices used for preterm labor, preterm premature rupture of membranes, and IUGR.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jhosmerm">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kosovo</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Obstetrics and Gynecology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gtm mex egy northamerica africa anthropology socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Houston, Stephen</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Houston's research interests include Classic Mayan and South American archaeology, kingship and court systems; body concepts in antiquity; writing systems; epigraphy and decipherment; and architecture and urbanism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/shouston">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Egypt; Guatemala; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn twn asia eastasianstudies humanities" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hu, Lung-Hua</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Hu’s research focuses on Mandarin Chinese phonology and grammar as well as pedagogy. She has also been working on assessing study abroad programs designed for American college students located in China and Taiwan and on methods and effectiveness of incorporating technology in the teaching of Chinese.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/luhu">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Taiwan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher cub chn northamerica asia americanstudies history socialsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hu-Dehart, Evelyn</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Hu-DeHart's research focuses on the Asian diaspora in Latin American and the Caribbean with the goal of uncovering and recovering the history of Asian migration, and to document and analyze the contributions of these immigrants to the formation of Latin/Caribbean societies and cultures.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ehudehar">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Cuba</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>American Studies; History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dnk idn europe asia geologicalsciences physicalsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Huang, Yongsong</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Huang specializes in the development and application of organic isotope geochemistry and organic analytical chemistry to problems in the environment, paleoclimate, paleohydrology, and astrobiology. Currently funded research comprises projects on Greenland, Sub-Saharan Africa, and Indonesia.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/yohuang">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Denmark; Indonesia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher aus oceania engineering physicalsciences" data-lastname="H">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Hurt, Robert</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Hurt’s research focuses on nanomaterials and their applications and implications for human health and the environment. He was visiting professor at the University of Sydney in 2002.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rhurt">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Australia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia brainandneuralsystems " data-lastname="I">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Intrator, Nathan </h4>
		<p class="researcher-title">Adjunct Professor</p>
		<div class="researcher-description"><p>Intrator's research includes neural computation, machine learning, pattern recognition, and model estimation.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nintrato">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Brain and Neural Systems</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia familymedicine biology&medicine" data-lastname="I">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ip, Julianne</h4>
		<p class="researcher-title">Clinical Associate Professor</p>
		<div class="researcher-description"><p>Ip is interested in International Medicine. She has been an Associate Dean of Medicine since 1985.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jipmd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher arg southamerica sociology socialsciences" data-lastname="I">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Itzigsohn, Jose</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Itzigsohn’s work focuses on identity and group formation, with a focus on processes of racialization, and ethnic and nation formation. He is also interested in the political economy of inequality. He is currently writing a book on industrial democracy in Argentina, studying the organizational forms of the worker-owned factories.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jitzigso">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Argentina</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra tun mar civ gab europe africa frenchstudies humanities" data-lastname="I">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Izzo, Justin</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Izzo was trained in both literary studies and cultural anthropology. He is particularly interested in the history of French anthropology and its relationship to fiction; literature and film from the Caribbean, West Africa, and the Indian Ocean; and representations of race in the novel.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jizzo">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Côte d'Ivoire; France; Gabon; Morocco; Tunisia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia eastasianstudies humanities" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Jackson, Yuko</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Jackson teaches Japanese at Brown. Her primary interests are social linguistics, pragmatics, and teaching methodology, including effective incorporation of computer assisted learning into the curriculum and intercultural communication strategies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/yjackson">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia judaicstudies humanities" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Jacobson, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Jacobson’s research focuses on the relationship of modern Hebrew literature to the Jewish tradition. His more recent research has focused on the relationship of contemporary Israeli poetry to the Jewish tradition.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/djacobso">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Judaic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr chn europe asia  lifesciences" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Jacobson, Pauline</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Jacobson has taught at the European Summer School in Language, Logic, and Information and at the East Asian School in Language, Logic, and Computation. Her research focusses on the formal tools needed to model the syntactic and semantic systems of natural languages, with special emphasis on the interaction of syntax and semantics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pjacobso">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fin esp che gbr can europe northamerica ecologyandevolutionarybiology biology&medicine" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Janis, Christine</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Janis primarily focuses on the evolutionary history of large mammals. She is currently working with the University of Helsinki to combine Eurasian data with North American data concerning mammalian community patterns over the past 20 million years.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cjanis">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Canada; Finland; Spain; Switzerland; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia pediatrics biology&medicine" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Jenny, Carole</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Jenny works with crash test dummies at the Aprica Child Care institute to study the biomechanics of accidental and inflicted head trauma in infants.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/carjenny">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra che jpn europe asia physics physicalsciences" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Jevicki, Antal</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Jevicki's interests comprise Quantum Field Theory (String Theory, Quantum Gravity, Black Holes, Non-perturbative and Collective Phenomena). More recently he has carried out research at CERN (Geneva), ITP (Santa Barbara), École Normale Supérieure (Paris), The Yukawa Institute (Kyoto), and the University of Tokyo.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/anjevick">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Japan; Switzerland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe music humanities" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Josephson, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Josephson’s current research examines the forced emigration of musicians and music scholars from Nazi and Fascist Europe from 1933 to 1945. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/djosephs">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jor tur syr psx irn asia anthropology socialsciences" data-lastname="J">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Joukowsky , Martha</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Joukowsky researches the domestication of plants and animals, the advent of Middle East urbanism and the varied cultural developments in Anatolia (ancient Turkey), Syro-Palestine, and ancient Iran. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/msjoukow">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Iran; Jordan; Palestine; Syria; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kahn, Coppelia</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kahn’s current research concerns race and national identity in 20th-century English and American constructions of Shakespeare.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ckahn">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind ken asia africa medicine biology&medicine" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kantor, Rami</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Kantor's research focuses on the evolution of HIV drug resistance to antiretroviral medications, which jeopardizes treatment success. More specifically, he is studying the evolution of drug resistance in HIV variants that predominate in resource-limited settings and in developing countries, where the majority of the AIDS epidemic is located. He is the research director of the Brown University – Kenya program.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rkantor">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>India; Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe appliedmathematics physicalsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Karniadakis, George</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Karniadakis' research interests include diverse topics in computational science both on algorithms and applications. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/gkarniad">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Applied Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mdg africa environmentalstudies socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Karp, Caroline</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Karp's areas of interest are coastal and marine watershed management, water law, and urban environmental policy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ckarp">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Madagascar</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Environmental Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Katz, E Tamar</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Katz has research interests in 20th-century literature, with a focus on British modernism, urban literature, and gender studies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/etkatz">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Keach, William</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Keach has research interests in 18th- and 19th-century British literature and culture, including Blake, Wollstonecraft, Wordsworth, Coleridge, Byron, Shelley, Keats, and other writers in what is still called the "Romantic" tradition, as well as literary theory, historical materialism, and transatlantic literary culture.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wkeach">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher khm vnm lao asia behavioralandsocialsciences socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Keita, Akilah</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Akilah’s research focuses on identifying neighborhood contexts of diet, physical activity, obesity and obesity-related comorbidities. She is also interested in using mixed methods approaches to identify culturally appropriate childhood obesity interventions for under-served Asian populations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/adulinke">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cambodia; Lao PDR; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pan northamerica ecologyandevolutionarybiology biology&medicine" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kellner, James</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Kellner is interested in the structure of forests and trees over large geographic areas, and how they are changing over time, especially in tropical systems. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jrkellne">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Panama</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus pol europe sociology socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kennedy, Michael</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kennedy’s research areas include intellectuals and professionals in East European social movements and systemic change, cultural politics, knowledge networks and global transformations, attending especially to the European Union, nationalism, universities, social movements, and energy security in these terms.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mdkenned">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Poland; Russia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher sdn africa  lifesciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kertz, Laura</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Kertz conducts research in linguistics, with a focus on argument structure, information structure, and discourse. She is also a collaborator on a project documenting Moro, a language spoken in Sudan.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lkertz">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Sudan</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe anthropology italianstudies socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kertzer, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kertzer's research interests include Italian politics and history, anthropological demography, social organization, politics and symbols, political economy and family systems, age structuring, European historical demography and the history of Catholic Church-Jewish relations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dkertzer">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology; Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kervennic, Youenn</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Kervennic teaches French language classes with an emphasis on culture and civilization. He also serves as Resident Director for the Brown-in-France program in Paris.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ykervenn">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jor syr grc tur asia europe religiousstudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Khalek, Nancy</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Khalek’s research focuses on the formative period of Islamic history. Her other research interests include hagiography, biography and historiography in the Byzantine and Islamic worlds, relic and saint veneration, Christian-Muslim dialogue, and the relationship of material culture to religious life. Over the years, she has also conducted archaeological and ethnographic field work in Jordan, Syria, Greece and Turkey.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nkhalek">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Jordan; Syria; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Khalip, Jacques</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Khalip is interested in British Romanticism and queer studies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jkhalip">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc ita egy europe africa classics humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kidd, Stephen</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Kidd studies Greek literature with a focus on comedy. He is also internested in the concept of play, the concept of education ( paideia ), Hellenistic Egypt, and the comparison of ancient literatures beyond those of Greece and Rome.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sekidd">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Egypt; Greece; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher uga zaf africa behavioralandsocialsciences socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kiene, Susan</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Kiene studies behaviors that put individuals at risk for HIV/AIDS and methods for preventing HIV transmission. Her research program in rural Uganda aims to develop effective and sustainable methods to empower individuals for HIV/AIDS protection and transmission prevention.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/skiene">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>South Africa; Uganda</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia eastasianstudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kile, Sarah</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Kile specializes in early-modern Chinese literature and culture, examining Ming and Qing drama, fiction, and essays.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/skile">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor asia english humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kim, Daniel</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Kim's primary research field is 20th-century U.S. literature with a particular focus on the Asian American and African American traditions, ethnic studies, gender studies, and the Cold War.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dakim">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Korea</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor sgp asia engineering physicalsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kim, Kyung-Suk</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kim has played a central role in understanding and utilizing mechanical behavior of nanostructures by developing creative theories and conducting precision experiments. His collaborative research has received funds from Korea and Singapore.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kkim1">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Korea; Singapore</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor asia engineering physicalsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kim, Seunghyun</h4>
		<p class="researcher-title">Associate Professor Research</p>
		<div class="researcher-description"><p>Kim is interested in piezoelectric materials and energy applications. He is a participating member of various Korean committees, societies, and associations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sk22">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Korea</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf esp africa europe engineering physicalsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kingon, Angus</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kingon’s research focuses on entrepreneurship technology and commerce. He has been involved in the establishment of various technology entrepreneurship programs at universities in the UK, Portugal, Slovenia, Korea, Ireland, and South Africa.  He also serves as Academic Co-Director for the IE Brown Executive MBA Program.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/aikingon">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>South Africa; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nld europe economics socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kleibergen, Frank</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kleibergen researches the validity of statistical inference in economic models. His research has been funded by the Netherlands Organization for Scientific Research.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/fkleiber">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Netherlands</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe germanstudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kniesche, Thomas</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Kniesche's areas of expertise include modern German literature, intellectual history, literary theory, and psychoanalysis.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tkniesch">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia psychiatryandhumanbehavior " data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kohn, Robert</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kohn's research has focused on psychiatric epidemiology and geriatric psychiatry. He has conducted international research on the prevalence of mental illness, service utilization, stigma, social class, and schizophrenia, stress including natural disasters, terrorism, and immigration.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rkohnmd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe classics comparativeliterature humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Konstan, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Konstan's research has investigated the emotions and value concepts of classical Greece in Rome, and has written books on friendship, pity, the emotions, and forgiveness. He has also worked on ancient physics and atomic theory, and on literary theory.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dkonstan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics; Comparative Literature</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hti rwa ukr northamerica africa europe pediatrics biology&medicine" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Koster, Michael</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Koster’s area of interest is in physician decision making regarding diagnosis and testing of pneumonia and meningitis.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mkoster">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Haiti; Rwanda; Ukraine</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia languagestudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Koul, Ashok</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Koul's interests lie in the field of South Asian languages and linguistics. He is interested in syntax, as well as language teaching and learning. He has also co-authored a textbook, Colloquial Urdu, published by Routledge. His forthcoming books are Lexical Borrowings in Kashmiri, Hindi-Urdu for Beginners or Travelers, and The Colloquial English-Hindi/Urdu Wordbook.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/akoul">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Language Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc ita europe judaicstudies religiousstudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kraemer, Ross</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kraemer specializes in early Christianity and other religions of the Greco-Roman Mediterranean, including early Judaism. While much of her research focuses on aspects of women's religions in the Greco-Roman world, particularly Christian and Jewish women, her interests also extend to questions of theory and method in the academic study of religion, the study of women and religion cross-culturally and trans-historically, and even religion and modern media.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rkraemer">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Judaic Studies; Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra gbr europe politicalscience socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Krause, Sharon</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Krause works in the area of political theory. She has research interests in classical and contemporary liberalism; democratic theory; theories of freedom; the history of political thought; 18th-century studies (especially Hume and Montesquieu); political judgment and deliberative democracy; passions and politics; and feminism and political theory.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/srkrause">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Krause, Virginia</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Krause's research interests focus on Renaissance France, particularly early modern romance, the history of leisure, and witchcraft.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/vkrause">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa behavioralandsocialsciences socialsciences" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kuo, Caroline</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Kuo's research focuses on addressing mental health and HIV disparities among vulnerable populations through rigorous, evidence-based interventions that are culturally appropriate and sustainable in low-resource communities.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ck39">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher phl chn bra asia southamerica pathologyandlaboratorymedicine biology&medicine" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kurtis, Jonathan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Kurtis applies the techniques of molecular biology, immunology and population biology to identify vaccine candidates for both malaria and schistosomiasis in east Africa and the Philippines. By analyzing the relationship between specific immune responses and naturally acquired resistance in endemic populations, Kurtis identifies and characterizes new vaccine candidates. His current interests include the modulation of protective immune responses by nutritional and developmental factors in the human host and the identification of vaccine candidates for pediatric falciparum malaria.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jkurtis">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; China; Philippines</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pathology and Laboratory Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kuzner, James</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Kuzner has research interests in early modern literature and culture, critical theory, cognitive theory, and the histories of selfhood, sexuality, and skepticism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jkuzner">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gha africa medicine biology&medicine" data-lastname="K">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Kwara, Awewura</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Kwara's research is focused on understanding the molecular, biological and clinical factors underlying the inter-individual variability in HIV and TB treatment response in individuals with HIV and TB coinfection.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/akwaramd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Ghana</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kos europe obstetricsandgynecology biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">LaFontaine, Donna</h4>
		<p class="researcher-title">Clinical Associate Professor</p>
		<div class="researcher-description"><p>LaFontaine spent five days at the University Hospital of Kosovo where they perform approximately 10,000 deliveries a year. There she observed their models of care, education (as it is a teaching hospital as well), and led patient scenario conferences where they discussed the evidence based practices used for preterm labor, preterm premature rupture of membranes, and IUGR.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dlafonta">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kosovo</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Obstetrics and Gynecology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nzl oceania pediatrics biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lagasse, Linda</h4>
		<p class="researcher-title">Associate Professor Research</p>
		<div class="researcher-description"><p>Lagasse is currently studying children with prenatal exposure to methamphetamine and adolescents with prenatal exposure to cocaine.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/llagasse">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>New Zealand</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher che europe physics physicalsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Landsberg, Greg</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Landsberg does research in elementary particle physics, specifically experimental investigation of the fundamental particles and fields at the energy frontier accelerators. His main research activity is the search for new physics phenomena, including extra dimensions in space.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/glandsbe">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Switzerland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe philosophy humanities" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Larmore, Charles</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Larmore's work in moral and political philosophy has focused on such topics as the foundations of political liberalism, the nature of the self, and the nature of moral judgment. He has also published extensively on figures and problems in the history of philosophy, particularly in the area of 17th-century philosophy and on German Idealism, as well as on the nature of reason and reasons. He is currently at work on a book about the nature of freedom.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/clarmore">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra bol per ata southamerica antarctica geologicalsciences physicalsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lee, Jung-Eun</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Lee studies the global water cycle, focusing on how the terrestrial ecosystem influences and is influenced by the physical climate system. His recent works range from interpreting Antarctic temperatures around 15 Million years ago to quantifying the degree of water stress for Amazonian trees.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jl322">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Antarctica; Bolivia; Brazil; Peru</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn chn asia americanstudies socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lee, Robert</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Lee studies the history of Asians in the United States, racial formations, and relations between Asia and America.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rolee">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>American Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher per esp southamerica europe anthropology socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Leinaweaver, Jessaca</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Leinaweaver's research focuses on cultural anthropology and anthropological demography within Peru and the Peruvian diaspora. She has published on informal child fostering in the urban Andes, aging in Andean Peru, and transnational adoption and migration from Peru to Spain.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jleinawe">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Peru; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra can nld southamerica northamerica europe healthservicespolicyandpractice " data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lepore, Michael</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Lepore is the Director of Research for Planetree, an international network of healthcare organizations focusing on patient-centered care.  He has a number of research collaborations with hospitals and long-term care settings in Brazil, Canada, The Netherlands, and Sub-Saharan Africa.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mlepore">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Canada; Netherlands</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Health Services Policy and Practice</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica ecologyandevolutionarybiology environmentalstudies socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Leslie, Heather</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Leslie is interested in the ecological and social processes that link people and marine ecosystems, and how to more effectively integrate science into marine policy and management. Her research areas include coastal ecology, the design and evaluation of marine conservation and management strategies, and human-environment interactions.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hleslie">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology; Environmental Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nzl oceania pediatrics biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lester, Barry</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lester researches children at risk of poor developmental outcome based on both social factors, such as poverty, and biological factors, such as prenatal substance exposure.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/blesterp">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>New Zealand</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus ukr cze pol europe slaviclanguages humanities" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Levitsky, Alexander</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Levitsky's principal areas of research include 18th-Century and Modern Russian literature, Slavic Baroque (Russian, Ukrainian, Czech and Polish), Czech literature, theory and practice of translation.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/alevitsk">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Czech Rep.; Poland; Russia; Ukraine</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia comparativeliterature eastasianstudies humanities" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Levy, Dore</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Levy studies classical Chinese poetry and fiction, forms of narrative literature, and East Asian and European literature.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/djlevy">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn twn rus gbr asia europe education socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Li, Jin</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Li’s current research project studies how children (from preschoolers to college students) across cultures and ethnic groups develop learning beliefs, and how their beliefs influence their learning and achievement.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jili">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Russia; Taiwan; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Education</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher sgp chn asia theatreartsandperformancestudies humanities" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lim, Eng</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Lim teaches critical and performance theory, queer and intercultural theater, postcolonial and Anglophone drama, and Asian/American performance. He studies transnational Asian, American performance, literary and visual cultures that negotiate the boundaries of postcolonial, intercultural and diasporic stages. Some of his objects include global Anglophone drama, Southeast Asian dance-drama and Asian American performance. He has also written on the role of the arts, or the neoliberal cultural politics of the global university, particularly U.S.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/el46">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Singapore</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Theatre Arts and Performance Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe historyofartandarchitecture italianstudies humanities" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lincoln, Evelyn</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lincoln’s research on printing and printmaking in early modern Europe looks at how images were made to obtain and display access to knowledge, power, and patronage for communities of readers with a common literacy in image and text. Her work also comprises research into the larger visual culture of early modern Italy and the role and creation of imagery in all media.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/elincoln">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture; Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex gtm eth northamerica africa sociology socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lindstrom, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lindstrom’s research examines the determinants and consequences of migration in economically developing societies, the transition into adulthood, and the changing dynamics of reproductive health and behavior.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dlindstr">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Ethiopia; Guatemala; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica neuroscience biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lipscombe, Diane</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lipscombe is interested in the cellular mechanisms used to optimize calcium ion channel function. Calcium ion channels regulate many critical neuronal functions including transmitter release, nerve growth, and synaptic plasticity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dlipscom">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Neuroscience</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pr1 esp ita europe history socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Litchfield, Robert</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Litchfield studies Florentine early modern economic, social, and urban history. He is currently working on Portuguese and Spanish nobles at the Medici court in the 16th-17th centuries, and Florentine commercial relations through Livorno, Portugal, and Spain with the Atlantic World.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rlitchfi">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Italy; Portugal; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nld sgp europe asia computerscience physicalsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Littman, Michael</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Littman’s research focuses on machine learning, and examines algorithms for decision making under uncertainty.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mlittman">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Netherlands; Singapore</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Computer Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra deu europe engineering physicalsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Liu, Joseph</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Liu’s most recent research has focused on the role of longitudinal vortices in heat and mass transfer enhancement and in the promotion of rapid free mixing. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/joliu">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica epidemiology biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Liu, Simin</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Liu's research includes molecular epidemiology, germ-line mutation, cardiovascular risk, and diabetes susceptibility.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sl83">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa biostatistics biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Liu, Tao</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Liu investigates HIV/AIDS, and alcoholism and has participated in projects in Sub-Saharan Africa, mainly Kenya. (Cognitive-Behavioral Trial: Reduce Alcohol First in Kenya Intervention; Sub-Saharan Cancer Collaborative)</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tl7">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Biostatistics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica epidemiology biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Loucks, Eric</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Loucks' research areas include assessing causal associations between education and health, childhood family psychosocial environment, health and biological risk factors. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eloucks">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr swe aut isr ind europe asia economics socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Loury, Glenn</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Loury is an academic economist who has made scholarly contributions to the fields of welfare economics, income distribution, game theory, industrial organization, and natural resource economics. Loury has been a visiting scholar at Oxford, Tel Aviv University, the University of Stockholm, the Delhi School of Economics, the Institute for the Human Sciences in Vienna, and the Institute for Advanced Study at Princeton. He is a member of the Council on Foreign Relations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/gloury">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Austria; India; Israel; Sweden; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr mex asia northamerica physics physicalsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lowe, David</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Lowe's research primarily centers around applications of string theory to gravitational physics, including using string theory to understand questions in black holes physics and cosmology.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dlowe">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Israel; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hkg jpn asia americanstudies socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lubar, Steven</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lubar directs Brown's Humanities program. His research projects include work on museum history, material culture, and the digital humanities.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/slubar">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Hong Kong; Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>American Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa epidemiology biology&medicine" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lurie, Mark</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Lurie is working on HIV/AIDS, sexually transmitted infections and tuberculosis in Sub-Saharan Africa. He has studied the role of migration in the spread of HIV in South Africa; examined the evidence for concurrency as a major driver of the HIV epidemic; and studied the impact of antiretroviral therapy on HIV epidemic dynamics in Sub-Saharan Africa.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mlurieph">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gum jpn phl afg irq pak oceania asia anthropology internationalstudies socialsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lutz, Catherine</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lutz's research focuses on the military, war, and society; Cars, culture, and inequality; Race and gender; Democracy; Subjectivity and power; Photography and cultural history; Critical theory; Anthropological methods; Sociocultural contexts of science and technology; U.S. 20th-century history and ethnography.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/clutz">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Afghanistan; Guam; Iraq; Japan; Pakistan; Philippines</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology; International Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher aus oceania geologicalsciences physicalsciences" data-lastname="L">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Lynch, Amanda</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Lynch is the Director of the Environmental Change Initiative. She has provided advice to the Chief Scientist of Australia and the Presidential Science Advisor in the U.S. At present, Lynch is research associate of the Australian National University Institute for Water Economics and Environmental Policy, and member of the National Academy of Science Committee on Emerging Research Questions in the Arctic.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/al20">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Australia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra arm europe asia history judaicstudies socialsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mandel, Maud</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Mandel's research focuses primarily on the impact of policies and practices of inclusion and exclusion on ethnic and religious minorities in 20th-century France, most notably Jews, Armenians, and Muslim North Africans.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mmandel">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Armenia; France</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History; Judaic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn ind can asia northamerica engineering physicalsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mandre, Shreyas</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Mandre is a theorist researching problems in mechanics with applications to a wide array of fields. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/smandre">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Canada; India; Japan</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra esp europe physics physicalsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Maris, Humphrey</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Maris has a number of research interests including: studies of superfluid hydrodynamics using magnetically-levitated helium drops; quantum nucleation of bubbles in liquid helium at negative pressure; tests of quantum measurement theory using electrons in liquid helium; and ultrafast optical studies of solids including picosecond ultrasonic measurements on thin films and nanostructures.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hmaris">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica epidemiology biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Marshall, Brandon</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Marshall's research interests focus on substance use epidemiology and the social, environmental, and structural determinants of health of urban populations. In particular, his work seeks to inform public health and policy interventions that improve the health of drug users. He is also interested in applying complex systems methods to examine factors that perpetuate HIV transmission, and to identify interventions that reduce infectious disease spread in drug-using populations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bm8">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr aus can europe oceania northamerica physics physicalsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Marston, John</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Marston's research in the field of theoretical condensed matter physics focuses on materials and nanostructures with strong electronic correlations and/or driven away from equilibrium.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jmarston">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Australia; Canada; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe historyofartandarchitecture humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Martin, Courtney</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Martin specializes in modern and contemporary Art, 20th-century British art and sculpture studies. Her current research looks at art after 1968, the history of art criticism and exhibitions.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cm40">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe italianstudies humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Martinez, Ronald</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Martinez has published some forty scholarly essays on Dante, Petrarch, Boccaccio, Ariosto, and Machiavelli, co-authored a book on Dante's lyrics (Time and the Crystal: Studies in Dante's Rime Petrose, University of California Press, 1990) and translated Italian plays and literary criticism. In addition to Dante and Trecento Studies, his research interests include Renaissance drama and cultural history.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rlmartin">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu fra europe appliedmathematics physicalsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Maxey, Martin</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Maxey's research focuses on dynamical systems and partial differential equations and scaling laws.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mmaxey">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Applied Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind khm idn vnm asia epidemiology medicine biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mayer, Kenneth</h4>
		<p class="researcher-title">Adjunct Professor</p>
		<div class="researcher-description"><p>Mayer has been the principal investigator of an international training grant funded by the Fogarty International Center of the National Institutes of Health (NIH), which has led to new collaborations to define the natural history of HIV in India and to develop cost effective monitoring strategies for people living with HIV as they access generic antiretroviral therapy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kmayermd">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cambodia; India; Indonesia; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology; Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia history socialsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">McClain, James</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>McClain has taught and researched the history of early modern Japan at Brown for nearly a quarter century. He is currently researching a book to be entitled Tokyo Modern: The Dominance of the Middle-class in Twentieth Century Japan.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jmcclain">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gtm northamerica medicine biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">McCloy, Stephen</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>McCloy is interested in international health and needs assessment in Central America. He is also an occupational doctor.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/smccloym">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Guatemala</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pr1 europe music humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">McGarrell, Matthew</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>McGarrell performs and contributes to several Portuguese bands including the Sociedade Filarmónica de Covões and Banda Musical e Recreativa de Penalva do Castelo.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mmcgarre">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Portugal</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher wsm phl zaf gha oceania asia africa anthropology epidemiology biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">McGarvey, Stephen</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Director of the International Health Institute, McGarvey is concerned with issues of human population biology and international health, specifically modernization-related induced socio-economic and behavioral changes, gene by environment interactions on cardiovascular disease risk factors, tropical parasitology and child nutritional status and health, and environmental issues.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/smcgarve">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Ghana; Philippines; Samoa; South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology; Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr deu fra europe english humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">McLaughlin, Kevin</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>McLaughlin's research focuses on literature and philosophy in the 19th century.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kmclaugh">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe germanstudies humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mendicino, Kristina</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Mendicino's interests include the rhetoric of prophecy in German Idealism and Romanticism, translation, poetic and philosophical articulations of temporality, and choreography.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kmendici">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica comparativeliterature hispanicstudies humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Merrim, Stephanie</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Merrim has research interests in colonial Latin American historiography, the Baroque, early modern women's writing, and contemporary North and South American literatures.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/smerrim">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ago nam africa economics socialsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Michalopoulos, Stylianos G</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Stylianos Michalopoulus is currently researching effects of decentralization in African regions and economic measures of ethnic inequality.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/smichalo">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Angola; Namibia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe epidemiology biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Michaud, Dominique</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Michaud's research areas include the etiology of bladder, pancreas, and brain cancers, immune response, and genetic susceptibility.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dm15">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe classics humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mignone, Lisa</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Mignone studies Roman social history, especially the ongoing and interactive relationship of historical events and the sites in which they occur.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lmmignon">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher uga africa medicine biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Montague, Brian</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Montague’s current research focuses on the quality of care for patients living with HIV and developing metrics for evaluation of adequacy of linkage to care for persons released from prisons/jails to the community. As part of his work in Uganda, he is partnering with local investigators to address the problem of tuberculosis in pregnancy amongst inpatients at Mulago Hospital and to evaluate the impact of treatment for HIV in pregnancy on transmission of hepatitis B from mother to baby.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bmontagu">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Uganda</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia  biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mor, Vincent</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Mor is currently carrying out a study with Zhanlian Feng to develop and test a survey of nursing homes in China.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/vmor">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa behavioralandsocialsciences psychiatryandhumanbehavior biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Morrow, Kathleen</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Morrow’s research focuses on behavioral HIV/STI prevention interventions, and the development of biomedical products and devices for HIV/STI prevention.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kmorrowp">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences; Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher swe gbr nzl aus europe oceania ecologyandevolutionarybiology biology&medicine" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Morse, Douglass</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Morse researches host-parasitoid interactions, sexual selection, and behavioral ecology. He has been a visiting scholar at universities in Sweden, England, New Zealand, and Australia.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dmorse">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Australia; New Zealand; Sweden; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher lbn asia comparativeliterature humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Muhanna, Elias</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Muhanna is interested in the literature and culture of the Arab world, mainly classical Arabic literature and intellectual history. He has done extensive work on encyclopedism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/emuhanna">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Lebanon</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nld bel europe historyofartandarchitecture humanities" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Muller, Jeffrey</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Muller’s research centers on early modern art of the Low Countries; he is interested in how visual communication worked as part of the Counter Reformation.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jmmuller">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Belgium; Netherlands</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher per southamerica history socialsciences" data-lastname="M">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Mumford, Jeremy</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Mumford’s research focuses on the early colonial Andes and comparative indigenous histories in the New World. He works with Quechua speakers in South America.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jrmumfor">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Peru</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra ind southamerica asia economics environmentalstudies socialsciences" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Nagavarapu, Sriniketh</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Nagavarapu's research is focused on environmental and labor economics in developing countries; specifically, how local institutions manage natural resources and public service delivery, and how management effectiveness is shaped by market incentives and the nature of the institutions.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/snagavar">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; India</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics; Environmental Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher che europe physics physicalsciences" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Narain, Meenakshi</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Narain's research interests are in experimental high energy physics and her ultimate goal is to illuminate the character of physics at the TeV energy scale.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mnarain">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Switzerland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn twn asia history socialsciences" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Nedostup, Rebecca</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Nedostup is interested in the social and cultural impact of modern Chinese political movements; migration within and across borders and the development of communities amid displacement; mobilization and demobilization; and the changing conception and treatment of the dead in the modern era.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rnedostu">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Taiwan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica ecologyandevolutionarybiology biology&medicine" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Neil, Christopher</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Neill researches the ecological consequences of deforestation and development measures in the Amazon as well as locally in Massachusetts.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cneill">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu ita europe historyofartandarchitecture humanities" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Neumann, Dietrich</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Neumann’s research concentrates mostly on late 19th- and early 20th-century European and American Architecture. He is interested in the entire scope of architectural production and debate, from the minutiae of building technologies to the transatlantic discourse on skyscrapers and urbanism throughout the 20th Century.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dneumann">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia moderncultureandmedia humanities" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Neves, Joshua</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Neves's research explores global media formations and theory, with a particular interest in contemporary Asian media cultures. His work focuses on specific cities (e.g. Beijing), as well as comparative (e.g. intermedial, regional) approaches to media and sociality.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jneves">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Modern Culture and Media</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr fra europe comparativeliterature english humanities" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Newman, Karen</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Newman writes widely on early modern letters and culture and on Shakespeare and Renaissance drama. Her current research on the reception of Shakespeare in Europe and on cultural translation aims to historicize contemporary claims about the globalization of culture.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/knewman">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics humanities" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Nieto Hernandez, Pura</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Hernández teaches a variety of courses both in Greek and in translation. Her primary areas of research are Homer, the history of the Greek language, Greek literature and mythology.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pnietohe">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe history socialsciences" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Nummedal, Tara</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Nummedal's work examines knowledge of nature – particularly alchemy – and its place in the society and culture of early modern Europe.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tnummeda">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica behavioralandsocialsciences medicine biology&medicine" data-lastname="N">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Nunn, Amy</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Nunn's research is focused on examining the structural, social and behavioral factors influencing racial disparities in HIV infection in the US and the role of sexual networks contributing to racial HIV disparities.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/anunn">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences; Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus pol europe slaviclanguages humanities" data-lastname="O">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Oklot, Michal</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Oklot works in Russian and Polish literatures of the 19th and 20th centuries, especially Gogol and his continuators. He has also particular interests in Slavic history of ideas, Central European Modernism, and literary theory.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/moklot">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Poland; Russia</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Slavic Languages</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc deu europe classics humanities" data-lastname="O">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Oliver, Graham</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Oliver is an ancient historian. He studies Greek history, in particular epigraphy – the study of inscriptions, Classical and Hellenistic history, and the ancient economy and society of ancient Greece from the Archaic to Roman eras. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/goliver">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia judaicstudies religiousstudies humanities" data-lastname="O">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Olyan, Saul</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Olyan’s research focuses on the history, literature and religion of ancient Israel, and the history of biblical interpretation. He has written on aspects of disability, death and the afterlife, social hierarchy, sexuality, purity and impurity, honor and shame, and gender, among other subjects.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/solyan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Judaic Studies; Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn zaf ken mex asia africa northamerica behavioralandsocialsciences socialsciences" data-lastname="O">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Operario, Don</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Operario researches community-based health interventions for HIV/AIDS. His interests include social and psychological determinants of health inequalities, consequences of stigma, and evidence-based practice.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/doperari">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Kenya; Mexico; South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp mex europe northamerica hispanicstudies humanities" data-lastname="O">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ortega, Julio</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Ortega is currently spearheading the Trans-Atlantic Project at Brown. This is a group that works to advance research and collaboration between Spanish and Mexican Universities in areas related to “transatlantic studies.”</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jortega">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Mexico; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe engineering physicalsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Padture, Nitin</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Padture conducts research on nanomaterials and nanotechnology. His research has been funded by the government of Spain.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/npadture">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia chemistry engineering physicalsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Palmore, G Tayhas</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Palmore's research investigates the synthesis of new biocomposites (biocatalysts/conducting polymers/nanofibers) for use in batteries, fuel cells, and biomedical applications.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/trpalmor">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry; Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics humanities" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Papaioannou, Efstratios</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Papaioannou studies post-classical Greek literary and cultural history, especially late antique and Byzantine writing in its social context. Wider interests are in premodern book and letter-writing cultures, literary aesthetics, and concepts of gender, self, and desire.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/epapaioa">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa psychiatryandhumanbehavior biology&medicine" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Papas, Rebecca</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Pappas’s research focuses on cultural adaptation of empirically validated behavioral interventions, clinical trials of intervention to reduce alcohol use and gender health disparities. She also is involved in the International Health Institute and the Brown in Kenya Program.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rpapas">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fsm oceania emergencymedicine biology&medicine" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Partridge, Robert</h4>
		<p class="researcher-title">Adjunct Associate Professor</p>
		<div class="researcher-description"><p>Partridge is interested in medical counter-bioterrorism, anthrax prophylaxis, as well as substance dependence in elderly patients in emergency departments.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rapartri">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Micronesia</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Emergency Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr fra dnk asia europe physics physicalsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Pelcovits, Robert</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Pelcovits does research on theoretical liquid crystal physics, including numerical simulations and visualization. He has held visiting positions in Israel, Denmark, and France.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rpelcovi">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Denmark; France; Israel</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher idn mmr ind irl asia europe music humanities" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Perlman, Marc</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Perlman’s research interests include Indonesian, Irish and Burmese music, ethnomusicology, and music theory. He is currently working on the geographical disciplinary affiliations of traditional Indonesian music.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mperlman">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>India; Indonesia; Ireland; Myanmar</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra jam blz mex arg southamerica northamerica africanastudies socialsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Perry, Keisha-Khan</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Perry specializes in the critical study of race, gender, and politics in the Americas with a particular focus on black women's activism, urban geography and questions of citizenship, feminist theories, intellectual history and disciplinary formations, and the interrelationship between scholarship, pedagogy, and political engagement.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kyperry">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Argentina; Belize; Brazil; Jamaica; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Africana Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn kor asia eastasianstudies humanities" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Perry, Samuel</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Perry’s research examines the revolutionary cultures of 20th-century Japan and Korea, focusing on the proletarian avant-garde, children's literature and fiction about and by Koreans.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sperry">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Japan; Korea</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe chemistry biology&medicine" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Peti, Wolfgang</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>The main focus of Peti's research group is to understand the molecular mechanism that regulates signalling enzymes.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wpeti">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bgd asia populationstudies socialsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Pitt, Mark</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Pitt researches economic demography, resource allocation, fertility, and child mortality and health.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mpitt">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Bangladesh</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Population Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe history socialsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Pollock, Ethan</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Pollock has examined previously inaccessible Soviet archives to analyze the intersection of political power, official ideology, and scientific knowledge in the Soviet Union. His current research on the Russian bathhouse – or bania – explores questions of public and private space, sexuality, hygiene, and the body in the context of social and political upheaval and change.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/epollock">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe germanstudies " data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Poore, Carol</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Poore’s research focuses on 20th-century German literature and culture, literature and history, and disability studies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cpoore">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ven southamerica ecologyandevolutionarybiology environmentalchangeinitiative biology&medicine" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Porder, Stephen</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Porder’s work is focused on the interdisciplinary investigation of terrestrial ecosystems. He researches the consequences of human intervention in said ecosystems.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sporder">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Venezuela</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology; Environmental Change Initiative</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher sgp asia engineering physicalsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Powers, Thomas</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Powers’ research focuses on molecular and cellular biomechanics, the physics of soft matter, and non-linear dynamics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tpowers">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Singapore</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher lbr africa emergencymedicine biology&medicine" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Proano, Lawrence</h4>
		<p class="researcher-title">Clinical Associate Professor</p>
		<div class="researcher-description"><p>Proano’s research focuses on travel and international medicine. He has also published on tube thorascostomy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lproanom">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Liberia</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Emergency Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe classics humanities" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Pucci, Joseph</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Pucci has research interests in late antiquity, late Latin, medieval Latin, and comparative literary history with a focus on literary allusion and poetic genres. Interests are also in biography as a literary form (and as practitioner) and in literature and the American presidency.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jpucci">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia economics socialsciences" data-lastname="P">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Putterman, Louis</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Putterman’s research focuses on income distribution, growth rates, industrial behavior, employment, and preferences.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lputterm">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rabb, Melinda</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Rabb has research interests in literature and culture of the 'long' 18th century, that is, from the time of the English Civil Wars through the career of Jane Austen.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mrabb">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rambuss, Richard</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Rambuss's field is 16th- and 17th-century English literature and culture. He is mostly interested in Milton, Shakespeare, Spenser, and the metaphysical poets (especially Donne, Herbert, and Crashaw); devotional texts and images; and the Baroque.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rrambuss">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher khm vnm asia medicine biology&medicine" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ramratnam, Bharat</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Ramratnam’s focus is on defining the key cellular components that impact the replication of viruses such as HIV-1, herpes simplex virus (HSV) one and two, influenza, and hepatitis B/C. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bramratn">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cambodia; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ravillon, Stephanie</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Ravillon teaches French language and culture. Since 2009, she has been involved with the Research Center on Investment and International Trade ("Centre de Recherche sur le droit des investissements et des marchés internationaux", CREDIMI, University of Burgundy, France), which is affiliated with the French National Center for Scientific Research (CNRS), and has translated their annual Chronicle of the Informal Sources of International Business Law.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sravillo">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ravindranathan, Thangam</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Ravindranathan’s research interests are 20th- and 21st-century literature and criticism/theory; poetry; narratives of travel; the contemporary novel; the question of the animal; (post-war) representations of war. Her current book project, Animals Passing, focuses on animals in contemporary literary and critical thought.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/travindr">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Readey, Jonathan</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Readey is interested in nonfiction writing and the research essay; creative nonfiction; journalism; business and technical writing; 20th-century and contemporary British and British commonwealth literature, especially the novel; Anglophone, World English, and transatlantic literature; postcolonial and colonial literature; Postmodernism and Modernism; creative writing, especially fiction.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jreadey">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra deu gbr europe comparativeliterature english humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Redfield, Marc</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Redfield studies British, American, French and German literature and literary theory of the 18th through 20th centuries, with a particular focus on romanticism and on the history, philosophy, and politics of post-romantic aesthetics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mredfiel">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc ita europe classics humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Reed, Joseph</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Reed is a scholar of Ancient Greek and Roman literature and culture, and has worked especially on Hellenistic and Augustan poetry.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jdreed">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe philosophy humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Reginster, Bernard</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Reginster's research has focused mostly on issues in ethics, metaethics, and moral psychology in 19th-century German philosophy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/breginst">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Reichman, Ravit</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Reichman does research in the 20th-century British novel; law and literature; modernism; literary theory; psychoanalysis; literature and the emotions; narrative and memory; and literary responses to war. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rreichma">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra mex esp europe northamerica history socialsciences" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Remensnyder, Amy</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Remensnyder's research focuses on the cultural and religious history of medieval Europe. The author of numerous articles and of a book about monastic culture and memory in southern France, she is currently finishing a book about how pre-modern Spanish Christians used the Virgin Mary as a symbol of the conquest and conversion of non-Christians in the Iberian Peninsula and in early colonial Mexico.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/aremensn">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Mexico; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher per ata southamerica antarctica ecologyandevolutionarybiology biology&medicine" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rich, Jeremy</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Rich’s work focuses on the microbial aspects of the nitrogen cycle. He has quantified rates of these processes in the Chesapeake Bay, Eastern Tropical South Pacific (Peru) and Arabian Sea.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jjr1">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Antarctica; Peru</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher rus europe epidemiology medicine biology&medicine" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rich, Josiah</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Rich has a number of research interests including: the overlap between infectious diseases and addiction; incarcerated populations and public health opportunities in the criminal justice system; HIV/AIDS, Hepatitis B and C, and sexually transmitted diseases; and global health.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jrichmph">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Russia</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology; Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe history socialsciences" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Richards, Joan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Richards is studying views of rationality as defined and lived in several generations of an English family that includes as major characters: the mathematician, Augustus De Morgan; his spiritualist wife, Sophia De Morgan; and Sophia's father, the radical Unitarian William Frend. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jorichar">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe engineering physicalsciences" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Richardson, Peter</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Richardson is currently working on a study of the left anterior descending epicardial arteries in pigs which have been fed atherogenic diets.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/perichar">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu fra europe comparativeliterature germanstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Richter, Gerhard</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Richter's research focuses on aesthetic theory and European critical thought since Kant; modern German literature and culture; the Frankfurt School; the intersections of literary writing and philosophy; contemporary French thought; questions of translation; and the complexities of intellectual and cultural inheritance.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/grichter">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra gbr ita southamerica europe italianstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Riva, Massimo</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Riva's research focuses on the role of emerging technologies in the humanities. His project, Garibaldi and the Risorgimento, centered on the Garibaldi panorama, was recently featured in the Italian pavilion in Brazil, the British Library in London, and museums and libraries in Italy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mriva">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Italy; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher khm asia familymedicine pediatrics biology&medicine" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rockney, Randy</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Rockney's research includes Family Medicine, Pediatric Incontinence Disorders, Disease in developing countries.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rrockney">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Cambodia</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine; Pediatrics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia history judaicstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rojanski, Rachel</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Rojanski is interested in Yiddish language and culture and the history of the State of Israel.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rrojansk">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History; Judaic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tur grc jor asia europe archaeologyandtheancientworld egyptologyandancientwesternasianstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rojas Silva, Felipe</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Rojas' research explores how people in Greek and Roman Anatolia used Bronze and Iron Age material culture to substantiate narratives about local and universal history.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/fr1">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Jordan; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Archaeology and the Ancient World; Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher asm oceania behavioralandsocialsciences socialsciences" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rosen, Rochelle</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Rosen is a medical anthropologist with primary interests in HIV-prevention in women via vaginal microbicide acceptability, preventative health care issues in Samoa and translational health research in behavioral medicine.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rrosen">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>American Samoa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia eastasianstudies religiousstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Roth, Harold</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Roth is a specialist in Classical Chinese Religious Thought, Classical Daoism, the Comparative Study of Mysticism and a pioneer of the academic field of Contemplative Studies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hroth">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies; Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe music humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rovan, Joseph</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Rovan is currently researching human-computer interface design and interactive opera and gestural control based on the work of French physiologist Etienne-Julers Marey.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jrovan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher tza cog bdi zmb uga idn africa asia geologicalsciences physicalsciences" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Russell, James</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Russell seeks to understand the patterns and causes of natural climate variability using paleoclimate records. He is particularly interested in the climate history of the tropics, including intertropical Africa and the El Niño Southern Oscillation System.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jarussel">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Burundi; Congo; Indonesia; Tanzania; Uganda; Zambia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher phl asia geologicalsciences " data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rutherford, Malcolm</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Rutherford’s research seeks to establish mineral stabilities and compositions of calc-alkaline magmas as a function of the fugacities of the volatile species, temperature, and the total pressure.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mrutherf">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Philippines</dd>

			<dt>Discipline:</dt>
			<dd></dd>
			
			<dt>Departments:</dt>
			<dd>Geological Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher irq syr tur asia egyptologyandancientwesternasianstudies humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Rutz, Matthew</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Rutz studies languages and cultures of ancient Mesopotamia.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mrutz">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Iraq; Syria; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="R">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ryan, Vanessa</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Ryan has research interests in 19th-century British literature and culture, cognitive science and the arts, and theories of knowledge in and outside of the humanities.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/vryan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics history socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sacks, Kenneth</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Sacks’ area of specialty is Greek intellectual history, which led him to study American Transcendentalism. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ksacks">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Classics; History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra ecu bra europe southamerica history socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Safier, Neil</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Safier researches 18th-century European empires and their overseas territories, especially the Andes, the Amazon River area, and Brazil.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nsafier">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Ecuador; France</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe comparativeliterature frenchstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Saint-Amand, Pierre</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Saint-Amand has research interests in 18th-century literature, especially the libertine novel, the philosophy of the Enlightenment, the French Revolution, and literary criticism and theory.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/psaintam">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr psx asia judaicstudies religiousstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Satlow, Michael</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Satlow is engaged in two major research projects: a synthetic Jewish history of the Second Temple period (ca. 520 BCE - 70 CE) and a study of a Jewish piety in late antiquity. The latter especially dovetails with his interest in inscriptions from Israel/Palestine, for which he is developing a digital database.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/msatlow">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Israel; Palestine</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Judaic Studies; Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia economics socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sautmann, Anja</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Sautmann's research interests cover a range of topics in microeconomics and development economics. She has studied the "inflation" of dowry payments and the connection with the increase in population growth in India, as well as the role of age preferences for marriage age patterns. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/asautman">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr ita grc europe comparativeliterature humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Saval, Peter</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Saval studies the classical tradition in Renaissance literature, with particular emphasis on Renaissance Platonism. He is also interested generally in the relationship between early modern literature and philosophy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/psaval">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Italy; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia eastasianstudies religiousstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sawada, Janine</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Sawada specializes in the religious and intellectual history of early modern Japan. She is currently studying the evolution in the seventeenth and eighteenth centuries of ideas and practices related to the worship of Mt. Fuji.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jsawada">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies; Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc esp europe ecologyandevolutionarybiology environmentalstudies biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sax, Dov</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Sax's research examines how species invasions and climate change impact native species and ecosystems. He is particularly interested in understanding when species extinctions are likely and what strategies we can take to prevent them from occurring.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dsax">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Greece; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Ecology and Evolutionary Biology; Environmental Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc europe classics humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Scafuro, Adele</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Scafuro is broadly interested in Greek literature and history, especially in the intersections of law, civic institutions, social life, and performance.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ascafuro">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Greece</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Classics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher isr asia philosophy humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Schechter, Joshua</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Schechter’s current research focuses on explaining how it is we have a priori knowledge - knowledge that is independent of the content of our experience of the world around us.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jschecht">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Israel</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Philosophy</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex gtm northamerica anthropology socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Scherer, Andrew</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Scherer is an anthropological archaeologist and biological anthropologist with a geographic focus in Mesoamerica (Maya). He co-directs a multi-national, interdisciplinary research project that is exploring Classic Maya polities along the Usumacinta River in Guatemala and Mexico. Scherer's research interests include bioarchaeology, mortuary archaeology, warfare and violence, political organization, diet and subsistence, and landscape archaeology.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/as49">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Guatemala; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu fra europe germanstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Schestag, Thomas</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Schestag's research interests include the relation between literature and philology; theories of names and naming; the intersection of poetry, philosophy, and political theory; the theory and practice of translation; and the question of a sense for language that precedes the preconceptual notion of a linguistic common sense.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tschesta">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex jpn northamerica asia  lifesciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Schloss, Karen</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Schloss studies aesthetics, especially color aesthetics. She has also studied cross-cultural color preferences, mainly comparatively.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kschloss">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Japan; Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia religiousstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Schopen, Gregory</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Schopen’s research focuses on Buddhist religion and philosophy. He is the author of Figments and Fragments of Mahayana Buddhism in India (2005), among other books.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/gschopen">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Religious Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Schuhmacher, Nidia</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Schuhmacher’s areas of interest include language pedagogy, the use of technology in language instruction, second language acquisition, the teaching of writing and culture, and translation. She has sponsored Group Independent Study Projects in Medical Spanish and is the Faculty Adviser Medical Spanish, an elective course offered by the Brown Alpert Medical School.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nschuhma">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Schultz, Gretchen</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Schultz’s research focuses on 19th-century French literature and culture, and gender and sexuality studies. She has also written on French poetry and poetics in relation to gender and subjectivity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/gschultz">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf africa psychiatryandhumanbehavior biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Scott-Sheldon, Lori</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Sheldon’s research explores evaluation of the conditions under which people adopt and continue risky behavior, health messages and behavior change, and social cognitive processes related to health behavior.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lscottsh">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Seifert, Lewis</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Seifert's primary interests encompass two broad areas: 17th-century French culture and folk- and fairy-tale narratives. He has a particular focus on questions of gender and sexuality, both theoretically and historically; but he has also explored problems of early modern authorship, political discourse, and friendship.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lseifert">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp deu isr europe asia economics socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Serrano, Roberto</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Serrano’s research interests include economic theory and game theory, welfare economics and social choice theory. He has researched with Spanish, German, and Israeli foundations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rserrano">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; Israel; Spain</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Economics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe  lifesciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Serre, Thomas</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Serre's research focuses on understanding the brain mechanisms underlying the recognition of objects and complex visual scenes using a combination of behavioral, imaging and physiological techniques.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/tserre">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra isl europe music humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Shapiro, Gerald</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Shapiro is currently a composer of acoustic and electronic music interested in areas of orchestration and the manipulation of time in an expressive context. He has collaborators at the Icelandic Symphony Orchestra and the Ensemble Zelig in Paris where he has premiered his work. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/gmshapir">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>France; Iceland</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica pediatrics psychiatryandhumanbehavior biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sheinkopf, Stephen</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Sheinkopf’s research focuses the early identification of autism, and on the development of social abilities and regulatory capacity in young children with autism, as well as children at risk for poor social developmental outcomes.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ssheinko">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics; Psychiatry and Human Behavior</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe engineering physicalsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sheldon, Brian</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Sheldon's current research is focused on understanding stress evolution and mechanical degradation in electrodes for Li ion batteries, compositionally induced stresses due to oxidation-reduction in non-stoichiometric oxides such as ceria, and ceramic composite coatings reinforced with nanotubes and nanofibers. He was recently Invited Visiting Professor at the Institut National Polytechnique de Toulouse.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bsheldon">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia americanstudies history socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Shibusawa, Naoko</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Shibusawa studies U.S. empire and political culture, as well as transnational Asian-American history.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nshibusa">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>American Studies; History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn lso zaf asia africa sociology socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Short, Susan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Short is a sociologist and demographer. Her research focuses on gender, reproductive health, child development, work-family issues, and methods of social research. She addresses these topics in the U.S., China, and Lesotho.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sushort">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Lesotho; South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu europe sociology urbanstudies socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Silver, Hilary</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Silver's research interests include urban poverty, housing and homelessness, neighborhood change, unemployment, and social policies in Western Europe.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hsilver">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Germany</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology; Urban Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pr1 europe portugueseandbrazilianstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Simas-Almeida, Leonor</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Simas-Almeida's teaching and research cover different periods and different genres of Portuguese literature. However, her work is particularly centered on narrative fiction and, lately, it has been reflecting a strong interest in a poetics of reading rooted in affect (although not excluding other compatible approaches), which inform all of her most recently published papers.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lsimasal">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Portugal</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia  lifesciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Simmons, Andrea</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Simmons’ research studies how the nervous system develops, matures, and reorganizes in response to damage. She uses frogs as animals systems for her research.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ansimmon">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher sen africa anthropology socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Simmons, William</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Simmons studies social/cultural anthropology, religion, myth, and ritual – focusing on West Africa and Native America. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wssimmon">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Senegal</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu gbr europe  lifesciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sloman, Steven</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Sloman studies how people think in the face of uncertainty. With the goal of understanding the flexibility of cognition - how people are able to think effectively in entirely new situations with little prior knowledge.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ssloman">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher nga africa anthropology socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Smith, Daniel</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Smith's research is focused on medical and demographic anthropology in sub-Saharan Africa and includes work on HIV/AIDS, reproductive health and behavior, adolescent sexuality, marriage, kinship, and rural-urban migration. His work on political culture in Nigeria includes studies of patron-clientism, Pentecostal Christianity, vigilantism, and corruption.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/djsmith">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Nigeria</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia history socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Smith, Kerry</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Smith research explores the social and cultural histories of the Great Kanto Earthquake of 1923, an event that devastated most of present-day Tokyo and many of the surrounding communities.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ksmith1">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Smith, Victoria</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Smith is interested in foreign language teaching and contemplative studies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/vsmith">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia americanstudies socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Smulyan, Susan</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Smulyan researches popular culture, transnationalism, and American-Japanese relations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ssmulyan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>American Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr deu europe comparativeliterature germanstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sng, Zachary</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Sng works on the literature and philosophy of Britain and Germany around the 18th century, covering the intellectual and literary movements of the Enlightenment and Romanticism.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/zsng">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica politicalscience socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Snyder, Richard</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Snyder's research and teaching focus on comparative politics, with an emphasis on the political economy of development, political regimes, and Latin American politics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rsnyder">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe  lifesciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sobel, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Sobel studies how children represent and learn new pieces of causal knowledge, how children conceptualize pretending and fictional worlds, and how children learn in informal settings.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dasobel">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica portugueseandbrazilianstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sobral, Patricia</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Sobral's teaching and research interests include: intersection of the arts and second language acquisition; Brazilian literature of the 20th century; contemporary Brazilian cinema; and comparative literature, particularly texts of immigration and exile.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/psobral">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sobral, Silvia</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Sobral teaches courses including Basic and Advanced Spanish as well as Theory and Methods of Foreign Language Teaching. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ssobral">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher aut europe germanstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sokolosky, Jane</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Sokolosky’s research focuses on language pedagogy; language learning and technology; writing in the language curriculum; turn-of-the-century Austrian literature.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jsokolos">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Austria</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>German Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica  lifesciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Song, Joo-Hyun</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Song is interested in understanding behavioral and underlying neural mechanisms involved in integrating higher-order cognitive processes and visually-guided actions in real-life situations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/js21">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica behavioralandsocialsciences socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Spillane, Nichea</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Spillane's research focuses on risk and protective factors for substance abuse in North American indigenous populations.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nspillan">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Behavioral and Social Sciences</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chl bol jpn southamerica asia internationalstudies socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Stallings, Barbara</h4>
		<p class="researcher-title">Professor Research</p>
		<div class="researcher-description"><p>Stallings' research focuses on the political economy of development, with particular reference to three topics: (1) macroeconomic policy and structural reforms and their impact on growth, employment creation, and income distribution; (2) finance for development, including domestic banks, capital markets, and international finance; and (3) the international political economy and its effect on the development process. The main geographic focus of her research has been on Latin America and East Asia.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/bstallin">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Bolivia; Chile; Japan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>International Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe english humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Stanley, Lawrence</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Stanley researches rhetorical theory and its relevance to composition theory and practice; narrative theory, particularly in relation to creative nonfiction and fiction; British Romantics and Modernist fiction.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lstanley">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>English</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher irq syr tur asia egyptologyandancientwesternasianstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Steele, John</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Steele specializes in Mesopotamian astronomy and the history of astronomy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jmsteele">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Iraq; Syria; Turkey</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Egyptology and Ancient Western Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dom uga northamerica africa medicine biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Stein, Michael</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Stein focuses on HIV prevention and drug use. He collaborates with initiatives in the Dominican Republic and Uganda.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/msteinm">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Dominican Rep.; Uganda</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher deu aut europe history music humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Steinberg, Michael</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Steinberg is currently researching the cultural history of modern Germany and Austria with regard to the German Jewish intellectual history and cultural history of music.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mpsteinb">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Austria; Germany</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History; Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia politicalscience socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Steinfeld, Edward</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Steinfeld's research focuses on the political economy of contemporary China, the political economy of global production and innovation, and the political economy of energy.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/es73">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita deu europe italianstudies humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Stewart-Steinberg, Suzanne</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Stewart-Steinberg works on Italian and German literature of the 19th and 20th centuries, and on psychoananlysis and literary and cultural theory with particular interests in the construction of gender.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sstewart">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Germany; Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Italian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher egy africa anthropology socialsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Straughn, Ian</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Straughn's research and teaching interests focus on the study of the archaeology of the Muslim World. More specifically, his work concentrates on understanding how Muslim societies have been shaped by the landscapes which they have constructed and conceived and by their relationship to the material world. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/istraugh">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Egypt</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher irl fra chn europe asia mathematics physicalsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Strauss, Walter</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Strauss’ research is devoted to understanding the fundamental underlying features of nonlinear waves and their relationships to physical phenomena.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wstrauss">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; France; Ireland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Mathematics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken tza ind africa asia surgery biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sullivan, Patrick</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Sullivan spent time at Tenwek Hospital in Kenya performing surgery and teaching physicians and staff about specialized medical care for children and adults.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/psulliva">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>India; Kenya; Tanzania</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Surgery</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hti northamerica pediatrics surgery biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sullivan, Stephen</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Sullivan is interested in pediatric plastic surgery, cleft lip and palate, and craniofacial plastic surgery.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/srsulliv">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Haiti</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics; Surgery</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher twn asia pathologyandlaboratorymedicine biology&medicine" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Sung, James</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Sung is interested in tumor pathology within the areas of gynecological pathology, breast pathology, and cytopathology.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/csungmd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Taiwan</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pathology and Laboratory Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher est europe engineering physicalsciences" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Suuberg, Eric</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Suuberg’s research interests focus on energy and environmental engineering. He also co-founded and co-directs the master's Program in Innovation Management and Entrepreneurship (PRIME).</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/esuuberg">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Estonia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe literaryarts humanities" data-lastname="S">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Swensen, Leigh</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Swensen is the author of fourteen volumes of poetry and a collection of critical essays. She is the founding editor of the translation press "La Presse." She is a translator of contemporary French poetry, prose, and art criticism, and won the 2004 PEN USA Award in Literary translation. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/cswensen">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Literary Arts</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia eastasianstudies humanities" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tajima, Hiroshi</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Tajima teaches Japanese at Brown.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/htajima">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe computerscience physicalsciences" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tamassia, Roberto</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Tamassia researches information security, cryptography, algorithms, and computational geometry. He serves regularly on program committees of international conferences.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rtamassi">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Computer Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher che jpn europe asia physics physicalsciences" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tan, Chung-I</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Tan's research interest is in theoretical particle physics, which deals with the fundamental understanding of the origin of forces and particles.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/chtan">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Japan; Switzerland</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor rus gbr asia europe visualart humanities" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tarentino, Leigh</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Tarentino is a visual artist who makes paintings, works on paper and digital prints constructed from photographs of the built landscape. Her work has been exhibited in New York, London, Seoul, Moscow and London.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ltarenti">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Korea; Russia; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Visual Art</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia medicine biology&medicine" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tashima, Karen</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Tashima is a professor of medicine in the Division of Infectious Diseases. She is the Director of HIV Clinical Studies and Clinical Research Site Leader of the AIDS Clinical Trials Unit at The Miriam Hospital. Tashima is the study chair of an ACTG study evaluating the effectiveness of a new strategy to treat HIV-infected persons with drug resistant virus. She is a principal investigator for studies of new medications for hepatitis C infection. She assisted in the establishment of an ACTG clinical trials site in Chennai, India at YRG Care.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ktashima">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher hti northamerica pediatrics surgery biology&medicine" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Taylor, Helena</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Taylor is co-director of the craniofacial program at Hasbro Children's Hospital and Rhode Island Hospital. She has a broad plastic surgery practice, with a special focus on pediatric plastic surgery, and in particular Craniosynostosis. Her research is focused on evaluation of craniofacial diagnoses and outcomes, often using three-dimensional photogrammetry.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ht1">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Haiti</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pediatrics; Surgery</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe familymedicine biology&medicine" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Taylor, Julie</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Taylor serves as the Director of Clinical Curriculum at the Alpert Medical School's Department of Family Medicine and offers a 3-week independent study in Spain.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jtaylorm">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pol ltu europe history judaicstudies humanities" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Teller, Adam</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Teller is an early modern historian, specializing in the history of the Jews in the Polish-Lithuanian Commonwealth. He focuses on the ways in which they became an integral part of society there and the tensions this aroused.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/at22">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Lithuania; Poland</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History; Judaic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Thomas, Sarah</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Thomas's work primarily focuses on the cultural production of contemporary Spain in its historical and political context. She has a secondary interest in Latin American literature and film of the same period, and the transatlantic dialogues that emerge between cultures in the Spanish-speaking world.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/st10">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn twn asia eastasianstudies humanities" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tseng, Hsin-I</h4>
		<p class="researcher-title">Lecturer</p>
		<div class="researcher-description"><p>Tseng teaches Chinese at Brown.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ht7">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Taiwan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra per southamerica music humanities" data-lastname="T">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Tucker, Christopher</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Tucker’s research centers on sound and society, specifically popular music in the Andes and Brazil as well as cultural and international influences. His current work is focused on transnational circulation of Andean indigenous music and Peruvian indigeneity as well as Brazilian choro music in the late 19th century.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ctucker">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Peru</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Music</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica comparativeliterature portugueseandbrazilianstudies humanities" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Valente, Luiz</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Valente’s research interests include: Brazilian narrative of the 19th and 20th centuries; the relationship between fiction and history; the construction of national identity and the representation of the nation; and Brazilian poetry since 1945.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lvalente">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe anthropology archaeologyandtheancientworld humanities" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Van Dommelen, Peter</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Van Dommelen's research concentrates on later Mediterranean prehistory and Classical Antiquity throughout the first millennium BCE. Rural landscapes, colonialism and connectivity represent key themes of his research.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pvandomm">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology; Archaeology and the Ancient World</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ita europe historyofartandarchitecture humanities" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Vanel, Rebecca</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Molholt's area of focus is Roman art and architecture, and specifically the art of houses and baths in the Roman provinces.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rmolholt">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Italy</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica sociology socialsciences" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Vanwey, Leah</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>VanWey studies the dynamics of frontier settlement and consolidation in the Brazilian Amazon. She asks how we can simultaneously protect the Amazon's precious environmental resources while promoting equitable social and economic development.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lvanwey">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher esp europe hispanicstudies humanities" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Vaquero, Mercedes</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Vaquero’s research interests include the Medieval Spanish epic, chronicles, ballads, and oral tradition. She also regularly offers a graduate/undergraduate course on Spanish Philology.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mvaquero">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Spain</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind asia internationalstudies politicalscience socialsciences" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Varshney, Ashutosh</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Varshney’s research and teaching cover three areas: Political Economy of Development, Indian Politics, and Ethnicity and Nationalism. He is currently working on India's rising cities, the North-South economic divergence in India, and a multi-country project on cities and ethnic conflict.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/avarshne">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>India</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>International Studies; Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher bra southamerica portugueseandbrazilianstudies humanities" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Vieira, Nelson</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Vieira's teaching and research interests include: Brazilian narrative of the late 19th and 20th centuries; Brazilian and Latin American Jewish writing; ethnicity and the question of identity; diaspora and cultural studies; gender and literary theory; modernist and postmodernist theories; and literary translation.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/nvieira">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Portuguese and Brazilian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia comparativeliterature eastasianstudies humanities" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Viswanathan, Meera</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Viswanathan does research in classical Japanese poetry and prose; Western medieval court literature; and comparative poetics.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/mviswana">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe engineering physicalsciences" data-lastname="V">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Vlahovska, Petia</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Valhovska is a theorist interested in the mechanics of complex fluids. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pvlahovs">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher zaf ita africa europe  biology&medicine" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wands, Jack</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Wands directs the Liver Research Center, a newly constructed 13,000-sq. ft. facility that emphasizes studies relating to the molecular biology of liver diseases.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jwandsmd">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Italy; South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor asia eastasianstudies humanities" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wang, Hye-Sook</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Wang's research has focused on sociolinguistic and pragmatic aspects of Korean language acquisition.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/hwang">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Korea</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia chemistry physicalsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wang, Lai-Sheng</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Wang is an experimental physical chemist interested in the study of nanoclusters and solution-phase chemistry in the gas phase.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lw28">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia eastasianstudies humanities" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wang, Lingzhen</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Wang's areas of expertise include modern Chinese literature and culture, Gender Studies, Feminist Theory, and Chinese cinema.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/lwang">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn asia eastasianstudies biology&medicine" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wang, Yang</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Wang's primary research interests include the Chinese pragmatics, Chinese language pedagogy, and intercultural communication strategies.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/yawang">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>China</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher col jpn gtm per southamerica asia northamerica anthropology socialsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Warren, Kay</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Warren's research agenda has involved multi-sited ethnographic studies of counterinsurgency wars, community responses to violence and peace processes, indigenous intellectuals, the anthropology of multi-cultural democracies, and gender and politics. She also works on documentary film and media issues. Her most recent research on international norms, human trafficking, and foreign aid has focused on Colombia, Japan, and Washington. Her earlier projects focused on Guatemala and Peru. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kwarren">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Colombia; Guatemala; Japan; Peru</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr europe  lifesciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Warren Jr, William</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Warren is interested in the links between vision and action. He is a member of various international professional associations, and was a Visiting Lecturer at Oxford in 2008.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wwarrenj">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Life Sciences</dd>
			
			<dt>Departments:</dt>
			<dd></dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher dnk egy rus europe africa chemistry physicalsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Weber, Peter</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Weber’s research focuses on the development of new methods to investigate molecular reactions and then apply them to important model systems.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pweber">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Denmark; Egypt; Russia</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher swe dnk nor fra deu gbr europe comparativeliterature humanities" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Weinstein, Arnold</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Weinstein researches European and American narrative, Scandinavian literature, American fiction, literature and medicine, and the city theme in literature.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/arweinst">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Denmark; France; Germany; Norway; Sweden; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher arg bra southamerica politicalscience socialsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Weitz-Shapiro, Rebecca</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Weitz-Shapiro's research interests include comparative political institutions, political behavior, and the political economy of development, with a particular focus on Latin America.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rweitzsh">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Argentina; Brazil</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Political Science</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can chl northamerica southamerica epidemiology biology&medicine" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wellenius, Gregory</h4>
		<p class="researcher-title">Assistant Professor</p>
		<div class="researcher-description"><p>Wellenius studies the effects of air pollution on the risk of stroke and cardiovascular diseases.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/gwelleni">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Canada; Chile</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Epidemiology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher chn vnm gha ita asia africa europe sociology socialsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">White, Michael</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>White’s research focuses on issues that stand at the intersection of sociology and public policy. He studies the determinants of migration, urbanization, and their demographic and environmental consequences in developing countries such as China, Vietnam and Ghana.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/miwhite">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>China; Ghana; Italy; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Sociology</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken africa surgery biology&medicine" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">White, Russell</h4>
		<p class="researcher-title">Clinical Associate Professor</p>
		<div class="researcher-description"><p>White has been Chief of Surgery and Endoscopy at Tenwek hospital, Kenya, since 1997. Additionally, White has a program of research aimed at dealing with the endemic problem of esophageal cancer.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rwhitemd">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Kenya</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Surgery</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher cub northamerica comparativeliterature hispanicstudies humanities" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Whitfield, Esther</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Whitfield's current research focuses on war metaphors in Latin American political speech, literature and the arts; and on Welsh writing in the Americas. She has also worked extensively on Cuban culture of the post-Soviet period and on contemporary Latin American and Caribbean literature more broadly.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/eswhitfi">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Cuba</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Comparative Literature; Hispanic Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wiart, Annie</h4>
		<p class="researcher-title">Senior Lecturer</p>
		<div class="researcher-description"><p>Wiart’s professional interest lies mostly in the area of the theory and practice of second/foreign language teaching and learning, with an emphasis on teacher training. She has served as resident director of the Brown-in-France program many times.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/awiart">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia chemistry physicalsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Williard, Paul</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Williard’s research focuses on synthesis of marine natural products as well as vitamin D derivatives. He has a strong interest in the characterization of reactive intermediates. He was Fellow at the Japanese Society for the Promotion of Science in 2013.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pwilliar">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Chemistry</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe frenchstudies humanities" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wills, David</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Wills specializes in modernist literature as well as film theory and comparative literature. Wills is also known as a major translator and interpreter of the work of French philosopher Jacques Derrida.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/dwills">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>French Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ken dom ind khm vnm rus africa northamerica asia europe medicine biology&medicine" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wing, Edward</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Wing's research interests are focused on the immune host defenses against intracellular pathogens such as Listeria monocytogenes and Mycobacterium tuberculosis. He also has a strong clinical interest in infectious diseases, HIV, and international health.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/ewingmd">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Cambodia; Dominican Rep.; India; Kenya; Russia; Vietnam</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher grc bra ita pr1 europe southamerica anthropology historyofartandarchitecture socialsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Winkes, Rudolf</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Winke's areas of expertise include Roman portraiture, Greek sculpture, portraiture, Roman painting, and Greek &amp; Roman crafts, the impact of Greek and Roman culture on later periods and the art of emerging Christianity.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/rwinkes">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Greece; Italy; Portugal</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Anthropology; History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher gbr jpn europe asia education urbanstudies socialsciences" data-lastname="W">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Wong, Kenneth</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Wong’s research focuses on the politics of education, federalism, policy innovation, and governance redesign. His research has been funded by a number of international grants, mainly the British Council, and the Japan Society for the Promotion of Science.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kkwong">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Japan; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Education; Urban Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher kor chn can asia northamerica engineering physics physicalsciences" data-lastname="X">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Xu, Jingming</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Xu’s research focuses on nanotechnology, drug delivery, bone implant, photonics, and biomolecules. He has served and is serving as adviser and on advisory boards and committees of government agencies, labs, and companies and firms in the USA, Canada, Korea, and China.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/jxu1">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Canada; China; Korea</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering; Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher jpn asia eastasianstudies humanities" data-lastname="Y">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Yamashita, Kikuko</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Yamashita specializes in historical linguistics, Japanese linguistics, and language pedagogy. Her research interests include pragmatics, communication strategies, discourse analysis, and the language policies and national language of Japan.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kyamashi">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Japan</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>East Asian Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher can northamerica medicine orthopaedics biology&medicine" data-lastname="Y">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Yang, Wentian</h4>
		<p class="researcher-title">Assistant Professor Research</p>
		<div class="researcher-description"><p>Yang studies musculoskeletal disorders and diseases that cause cartilage degeneration or bone mineral loss.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/wy10">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Canada</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Medicine; Orthopaedics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica theatreartsandperformancestudies humanities" data-lastname="Y">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ybarra, Patricia</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Ybarra’s research interests include theatre historiography, Mexican theatre and performance, Latino/a theatre and performance, avant garde theatre, critical race studies, dramaturgy and directing.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/pybarra">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>Theatre Arts and Performance Studies</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fin deu fra gbr bra hkg europe southamerica asia physics physicalsciences" data-lastname="Y">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Ying, See-Chen</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Ying is interested in the development and application of analytical and computational methods to tackle problems with multiple time and length scales. Current research interests include the dynamics of DNA molecules in nanopores and nanochannels, as well as the generation of frictional force at the microscopic nanostructure level. Ying has held visiting appointments in Hong Kong, Finland, Brazil, Germany, the UK, and France.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/sying">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>Brazil; Finland; France; Germany; Hong Kong; United Kingdom</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Physics</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind pak asia history socialsciences" data-lastname="Z">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Zamindar, Vazira</h4>
		<p class="researcher-title">Associate Professor</p>
		<div class="researcher-description"><p>Zamindar is interested in cross-border and interdisciplinary histories for rethinking a divided South Asia, as well as the politics of violence and its impact on history-writing itself.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/vzaminda">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>India; Pakistan</dd>

			<dt>Discipline:</dt>
			<dd>Social Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>History</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher fra europe engineering physicalsciences" data-lastname="Z">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Zaslavsky, Alexander</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Zaslavsky’s research focuses on devices that could supplement the current silicon transistor-based microelectronics technology. </p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/azaslavs">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>France</dd>

			<dt>Discipline:</dt>
			<dd>Physical Sciences</dd>
			
			<dt>Departments:</dt>
			<dd>Engineering</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher ind lso zaf asia africa familymedicine biology&medicine" data-lastname="Z">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Zeller, Kimberly</h4>
		<p class="researcher-title">Clinical Assistant Professor</p>
		<div class="researcher-description"><p>Zeller's research focus is on the scale-up of HIV treatment and prevention of secondary HIV transmission in resource-restricted countries. Other interests include global research, training, and advocacy on HIV prevention and treatment. Recent projects include: leading a team to assist the Lesotho government in planning and funding the national response to HIV, developing of a program to train 100,000 private physicians in the Basics of HIV/AIDS treatment in India, investigating ongoing transmission risk in patients starting HIV/TB treatment in South Africa.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/kzeller">Research link</a></dt>
	
			<dt>Countries:</dt>
			<dd>India; Lesotho; South Africa</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Family Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher mex northamerica historyofartandarchitecture humanities" data-lastname="Z">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Zerner, Catherine</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Zerner's areas of interest include the history of art and architecture in Latin America, particularly with respect to the histories of landscape design, buildings, and cartography.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/czerner">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Mexico</dd>

			<dt>Discipline:</dt>
			<dd>Humanities</dd>
			
			<dt>Departments:</dt>
			<dd>History of Art and Architecture</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>
    

<li class="researcher pr1 europe pathologyandlaboratorymedicine biology&medicine" data-lastname="Z">
	<div class="researcher-name-bio">
		<h4 class="researcher-name">Zhitkovich, Anatoly</h4>
		<p class="researcher-title">Professor</p>
		<div class="researcher-description"><p>Zhitkovich studies DNA repair, damage, and stress signaling.</p>
</div>	
	</div><!-- end researcher-name-bio -->
	
	<div class="researcher-side">
		<dl>
			<dt><a href="https://vivo.brown.edu/display/azhitkov">Research link</a></dt>
	
			<dt>Country:</dt>
			<dd>Portugal</dd>

			<dt>Discipline:</dt>
			<dd>Biology & Medicine</dd>
			
			<dt>Departments:</dt>
			<dd>Pathology and Laboratory Medicine</dd>

			</dl>
	</div><!-- end researcher-side -->
	
</li>

    </ul>