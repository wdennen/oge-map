"use strict";

if (window.selectedfeature) { delete window.selectedfeature; }
if (window.map) { delete window.map; }

// set basic Leaflet map options

var selectedfeature = window.selectedfeature;
var map = window.map;

map = new L.map('map', {
    center: [30, 0],
    zoom: 2,
    minZoom: 2,
    maxZoom: 5,
    zoomControl: false,
    attributionControl: false,
    doubleClickZoom: true,
    scrollWheelZoom: false

});

// load tiles; we're using Brown's main web server for these
// to create the tiles: TileMil --> mb-tiles file --> mb-util export
L.tileLayer('//brown.edu/web/bill/tileserver/files/{z}/{x}/{y}.png', {
    id: 'oge-base',
    attribution: 'attribution line',
    maxZoom: 5,
    noWrap: true,
}).addTo(map);


// add control that shows state info on hover
// since we also have labels on hover, this is overkill and we should pick one
var info = L.control({
    position: "bottomleft"
});

info.onAdd = function(map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};

info.update = function(props) {
    this._div.innerHTML = (props ?
        'Viewing <strong>' + props.name + '</strong><br />' : '<strong>Choose a region or country</strong>');
};

info.addTo(map);


var resetbutton = L.control({
    position: "bottomright"
});

resetbutton.onAdd = function(map) {
    this._div = L.DomUtil.create('div', 'info2');
    this.update();
    return this._div;
};

resetbutton.update = function(props) {
    this._div.innerHTML = '<a class="leaflet-control-reset" href="javascript:doReset();" title="Reset">Reset</a>';
};

resetbutton.addTo(map);

// add the zoom control to the map, please it in the bottom right
// need to look into how to style this, if it's possible
new L.Control.Zoom({
    position: 'bottomright'
}).addTo(map);


function doReset() {
	map.setView([30, 0], 2);
	info.update();
	map.removeLayer(countryLayer);
	map.addLayer(continentLayer);
	if (selectedfeature) {
		countryLayer.resetStyle(window.selectedfeature.target);
		continentLayer.resetStyle(window.selectedfeature.target);
		selectedfeature='';
		}
	
	$('ul#faclist li').hide();	
	$('ul#faclist li').show();
	var hits = $('ul#faclist li').length;
	$('#hits').text(hits + ' Faculty Conducting Research Abroad' );

	history.pushState('', '', window.location.href.replace(window.location.search,'') );
	//document.getElementById("faculty-filter").reset();
}


var allCircles = L.layerGroup([]);

var ogeCircleMarker = L.CircleMarker.extend({
   options: { 
      code: '',
      name: ''
   }
});

function drawCircles(zoom) {
	for(var i=0; i<allCircles.length; i++) {
		//allCircles[i].remove();
	}
	
	$.getJSON( "json/country-counts-locations.json", function( data ) {
		$.each( data.countries, function( i, item ) {
			if( item.country.researchCount > 0 ) {
				var ccode = item.country.code.toLowerCase();
				if (item.country.longt ) {
				
					if (item.country.researchCount >= 30) {
						var myRadius = 12;
					} else if ( item.country.researchCount >= 5 && item.country.researchCount < 30 ) {
						var myRadius = 7;
					} else if (item.country.researchCount < 5 ) {
						myRadius = 5;
					}
				
					var myCircleMarker = new ogeCircleMarker([item.country.lat, item.country.longt], {
						fillColor: 'red',
						fillOpacity: 0.5,
						stroke: false,
						radius: myRadius,
						code: item.country.code.toLowerCase(),
						name: item.country.Title
					})
					.addTo(map);
					
					allCircles.addLayer(myCircleMarker);

					myCircleMarker.on("click", function () {
						//alert("code: "+myCircleMarker.options.code);
						if ( myCircleMarker.options.code != '' ){
							jumpToCountry( myCircleMarker.options.code );
						}
					});
					// add label
					myCircleMarker.bindLabel( myCircleMarker.options.name );
				}
			}
		});
	});
}


drawCircles(1);

console.log(allCircles);


function onEachCountry(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: chooseFeature
    });
    layer.bindLabel(feature.properties.name + "");
    
    layer._leaflet_id = 'country-' + feature.properties.adm0_a3.toLowerCase();
    //layer.brownid = 'country-' + feature.properties.adm0_a3.toLowerCase();
    //console.log(layer);
}


var countryLayer = L.geoJson(null, {
    // http://leafletjs.com/reference.html#geojson-style
    style: function(feature) {
        return {
            color: '#000',
            fillOpacity: 0,
            weight: 1,
            strokeOpacity: 1

        };
    },
    onEachFeature: onEachCountry
});


var runLayer = omnivore.topojson('json/countries.revised.topo.json', null, countryLayer)
    .on('ready', function() {
    	map.removeLayer(countryLayer);
        if (getUrlVar("id") != '' ) {
	        map.removeLayer(continentLayer);
        	jumpToCountry( getUrlVar("id") ) ;
        	map.removeLayer(continentLayer);
    	} else {
        	map.addLayer(continentLayer);
    	}
    })
    .on('error', function() {
        alert('Sorry, there was an error loading the GeoJSON file for countries.');
    })
	.addTo(map);

	function highlightFeature(e) {
		var layer = e.target;
		
		if ( window.selectedfeature && e.target != window.selectedfeature.target ) {
			layer.setStyle({
				weight: 5,
				color: '#d9223b',
				dashArray: '',		
				});
		}
		
		if (!L.Browser.ie && !L.Browser.opera) {
			layer.bringToFront();
		}
	
	}


function resetHighlight(e) {
    //countryLayer.resetStyle(e.target);  
	if ( e.target != window.selectedfeature.target ) {   
			var layer = e.target;
			countryLayer.resetStyle(e.target);  
	}
}

function chooseFeature(e) {
    
    if (window.selectedfeature) {
    	countryLayer.resetStyle(window.selectedfeature.target);
    	continentLayer.resetStyle(window.selectedfeature.target);
    }
    
    var countryid = e.target.feature.properties.adm0_a3.toLowerCase();

    window.selectedfeature = '';
    
    window.selectedfeature = e;

    // add custom zooms for specific countries
    // we do this because some countries are so big that we'd zoom all the way out otherwise
	switch (countryid) {
	case "usa":
		map.setView([39.82817500, -98.57950000], 4);
		break;
	case "can":
		map.setView([60.00000000, -96.00000000], 4);
		break;
	case "rus":
		map.setView([60, 100], 3);
		break;
	case "ata":
		map.setView([-75.552081, -4.218750], 4);
		break;
	case "nzl":
		map.setView([-42, 174.00000000], 4);
		break;
	case "fra":
		map.setView([46, 2], 5);
		break;
	default:
		map.fitBounds(e.target.getBounds());
		}
		
    var layer = e.target;
    	
    layer.setStyle({
        weight: 5,
		color: '#000',
        fillColor: '#fff',
        fillOpacity: .5,
    });
	
	info.update(layer.feature.properties);
	
	
	$('ul#faclist li').hide();	
	$('ul#faclist li.' + countryid).show();
	var hits = $('ul#faclist li.' + countryid).length;
	$('#hits').text(hits + ' Faculty Conducting Research in ' + layer.feature.properties.name );
	history.pushState({id:countryid}, '', "?id=" + countryid );
	
}


function toggleCountries() {
    if (map.hasLayer(countryLayer)) {
        map.removeLayer(countryLayer);
    } else {
        map.addLayer(countryLayer);
    }
}

function toggleContinents() {
    if (map.hasLayer(continentLayer)) {
        map.removeLayer(continentLayer);
    } else {
        map.addLayer(continentLayer);
    }
}



// ----------------

var continentLayer = L.geoJson(null, {
    style: function(feature) {
        return {
            color: '#000',
            fillOpacity: 0,
            weight: 1,
            strokeOpacity: 1
        };
    },
    onEachFeature: onEachContinent
});


var runLayer = omnivore.topojson('json/continents.topo.json', null, continentLayer)
    .on('ready', function() {
    	map.addLayer(continentLayer);
    })
    .on('error', function() {
        alert('Sorry, there was an error loading the GeoJSON file for continents.');
    })

.addTo(map);


function onEachContinent(feature, layer) {
    layer.on({
        mouseover: highlightContinent,
        mouseout: resetHighlightContinent,
        click: chooseContinent
    });
    layer.bindLabel(feature.properties.name + "");
    layer._leaflet_id = 'continent-' + feature.properties.cont.toLowerCase();
}



function highlightContinent(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#d9223b',
        dashArray: '',

    });

    if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
    }

    //info.update(layer.feature.properties);
}


function resetHighlightContinent(e) {
    continentLayer.resetStyle(e.target);
    info.update();
}

function chooseContinent(e) {
	var continentid = e.target.feature.properties.cont;
	
	if (window.selectedfeature) {
    	countryLayer.resetStyle(window.selectedfeature.target);
    	continentLayer.resetStyle(window.selectedfeature.target);
    }
        
    selectedfeature = e;

	var selected;
	
	switch (continentid) {
		case "africa":
			map.setView([7.307985, 20.039063], 3);
			selected = "africa";
			break;
		case "europe":
			map.setView([48.904449, 24.257813], 4);
			selected = "europe";
			break;
		case "northamerica":
			map.setView([38.779781, -98.437500], 3);
			selected = "northamerica";
			break;
		case "asia":
			map.setView([33.091542, 94.921875], 3);
			selected = "asia";
			break;
		case "southamerica":
			map.setView([-20.438595,-57.652128], 3);
			selected = "southamerica";
			break;
		case "oceania":
			map.setView([-22.695120, 132.890625], 3);
			selected = "oceania";
			break;
		case "antarctica":
			map.setView([-75.552081, -4.218750], 3);
			selected = "antarctica";
			break;
		default:
			map.fitBounds(e.target.getBounds());
	}

    var layer = e.target;
    	
    /*
    layer.setStyle({
        weight: 5,
        color: '#d9223b',
        dashArray: '',
        fillColor: '#800026',
        fillOpacity: .3,
        strokeOpacity: 1,
    });
	*/
	
	info.update(layer.feature.properties);
	
	map.removeLayer(continentLayer);
	map.addLayer(countryLayer);

	$('ul#faclist li').hide();
	
	$('ul#faclist li.' + continentid).show();
	var hits = $('ul#faclist li.' + continentid).length;
	$('#hits').text(hits + ' Faculty Conducting Research in ' + layer.feature.properties.name );
}


function jumpToCountry(code) {
	map.removeLayer(continentLayer);
	map.addLayer(countryLayer);
	map._layers['country-' + code ].fire('click');
}

function getUrlVar(key){
	var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search); 
	return result && unescape(result[1]) || ""; 
}

window.onpopstate = function(event) {
	if (event.state.id != '') {
		map._layers['country-' + event.state.id ].fire('click');
		//history.back();
	}
}

map.on('zoomend', function() {
    // here's where you decided what zoom levels the layer should and should
    // not be available for: use javascript comparisons like < and > if
    // you want something other than just one zoom level, like
    // (map.getZoom > 10)
	var zoom = map.getZoom();
	//drawCircles(zoom);
});


function doDiscipline(value) {
	alert(value);
}


/*
map.on('click', function(e) {
    doReset();
})
.on('dblclick', function(e) {
    // Zoom exactly to each double-clicked point
    map.setView(e.latlng, map.getZoom() + 1);
});
*/