<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<title>Map</title>
		
	<!-- using MapBox here, this includes Leaflet -->
	<script src='//api.tiles.mapbox.com/mapbox.js/v1.6.4/mapbox.js'></script>
	<link href='//api.tiles.mapbox.com/mapbox.js/v1.6.4/mapbox.css' rel='stylesheet' />
	
	<!-- leaflet-omnivore for digesting topojson -->
	<script src="leaflet-omnivore.min.js"></script>
	
	<!-- leadlet-label for displaying tooltips when hovering over countries -->
	<script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-label/v0.2.1/leaflet.label.js'></script>
	<link href='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-label/v0.2.1/leaflet.label.css' rel='stylesheet' />
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
	<link href="css/map.css" rel="stylesheet">

</head>
<body>


<div class="wrapper">

	<h1 id="country"></h1>

	<!-- container for our map -->
	<div id="map"></div>


	
<!-- 	<hr>	
	<a href="javascript:(jumpToCountry('bra'));">Choose Brazil</a><br />
	<a href="javascript:(jumpToCountry('cod'));">Choose Congo</a><br />
	<a href="javascript:(jumpToCountry('pak'));">Choose Pakistan</a><br />
	
	<p><hr></p>
	<a href="javascript:toggleContinents();">toggle continents</a><br>
	<a href="javascript:toggleCountries();">toggle countries</a><br>
	-->
	<?php include('faculty.php'); ?>

</div>


<script src="scripts/leaflet-default-tiles.js"></script>

</body>
</html>
